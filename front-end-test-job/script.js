const bigCircleId = 'bigCircle'

jQuery(document).ready(() => {
    //  Большая окружность будет являться "холстом"
    const bigCircleElement = document.getElementById(bigCircleId),
        context = bigCircleElement.getContext('2d'),
        bigCircleSize = {
            width: 320,
            height: 320
        },
        arcWidth = 34, // диаметр двигающейся окружности
        space = 1.2, // регулирует, начиная с какого расстояния окружность начинает убегать
        img = new Image() //фоновое изображение (окружности рисуемой Canvas'ом)

    img.src = './images/sm-circle-background.jpg'

    let centerPositionX = bigCircleSize.width / 2,
        centerPositionY = bigCircleSize.height / 2

    // Задание размеров большой ограничивающей окружности
    jQuery('#bigCircle').attr(bigCircleSize)

    // Очистка холста и отрисовка вложенной окружности
    const drawCircle = () => {
        context.clearRect(0, 0, 320, 320)
        let pattern = context.createPattern(img, 'repeat')
        context.fillStyle = pattern
        context.beginPath()
        context.arc(centerPositionX, centerPositionY, arcWidth, 0, Math.PI * 2, true)
        context.closePath()
        context.fill()
    }

    const moveCircle = (cursorX, cursorY, circlePositionX, circlePositionY) => {

        // Проверка расстояния по осям X и Y, от курсора до окружности
        // Если оба значения принимают true, то перерисовываем (передвигаем) окружность
        let checkX = (cursorX > (circlePositionX - arcWidth * space) &&
            cursorX < (circlePositionX + arcWidth * space))
            ? true : false

        let checkY = (cursorY > (circlePositionY - arcWidth * space)
            && cursorY < (circlePositionY + arcWidth * space)) ? true : false

        if (checkX && checkY) {

            let newPositionX, newPositionY

            // Определяем с какой по оси X стороны курсор и устанавливаем новый центр окружности по оси X
            if (cursorX < circlePositionX) {
                newPositionX = cursorX + arcWidth * space
            } else if (cursorX > circlePositionX) {
                newPositionX = cursorX - arcWidth * space
            }

            // Определяем с какой по оси Y стороны курсор и устанавливаем новый центр окружности по оси Y
            if (cursorY < circlePositionY) {
                newPositionY = cursorY + arcWidth * space
            } else if (cursorY > circlePositionY) {
                newPositionY = cursorY - arcWidth * space
            };

            // Проверяем возможно ли передвижение не выходя за границы, если возможно, то перерисовываем холст
            let checkPositionX = Math.pow((bigCircleSize.width / 2 - newPositionX), 2)
            let checkPositionY = Math.pow((bigCircleSize.height / 2 - newPositionY), 2)
            let diagonal = Math.sqrt(checkPositionX + checkPositionY)

            if (diagonal < bigCircleSize.width / 2 - arcWidth) {
                centerPositionX = newPositionX
                centerPositionY = newPositionY
                drawCircle()
            }
        }

        // Запись позиции курсора мыши и центра окружности
        jQuery('#pointerPosition')
            .text('Pointer coordinates: ' + cursorX + ':' + cursorY)
        jQuery('#circlePosition')
            .text('Circle center: ' + circlePositionX + ':' + circlePositionY)
    }

    const onMouseMove = (event) => {
        let x = event.offsetX == undefined ? event.layerX : event.offsetX
        let y = event.offsetY == undefined ? event.layerY : event.offsetY

        console.log(x)
        console.log(y)
        console.log(centerPositionX)
        console.log(centerPositionY)
        console.log("---------")

        moveCircle(x, y, centerPositionX, centerPositionY)
    }

    // Отслеживание передвижения курсора по большой окружности
    bigCircleElement.addEventListener('mousemove', onMouseMove)

    // Отслеживание выхода курсора за пределы большой окружности
    bigCircleElement.addEventListener('mouseout', () => {
        jQuery('#pointerPosition')
            .text('Pointer coordinates: move cursor on big circle and see the magic!')
    })

    drawCircle()
})