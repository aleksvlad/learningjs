import { animated, useSpring } from 'react-spring'
import React, { useState } from 'react'
import styled from 'styled-components'
import { useDrag, addV, useGesture } from 'react-use-gesture'

const Button = styled(animated.div)`
    position: absolute;
    left: 0;
    top: 0;
    width: 150px;
    height: 150px;
    margin: 100px;
    background-color: #c7539b;
    box-shadow: 0 15px 25px rgba(0,0,0,.35);
    border-radius: 8px;
`

const FlyingButton = () => {

    const [{ xy }, set] = useSpring(() => ({ xy: [0, 0] }))

    // const [{ deg }, set] = useSpring(() => ({ deg: 0 }))
    
    const [lastPosition, setLastPosition] = useState([0, 0])

    const bind = useGesture({
        onDrag: ({ movement }) => {

            const currPosition = addV(lastPosition, movement)

            set({
                xy: currPosition
            })

        },

        onDragEnd: ({ movement }) => {
            const [x, y] = addV(lastPosition, movement)

            setLastPosition([x, y])
        }
    })

    const transform = xy.interpolate((x, y) => {

        const halfWidth = window.innerWidth / 2
        const halfHeight = window.innerHeight / 2
        const xAngle = -(x - halfWidth) * .1
        const yAngle = -(y - halfHeight) * .1
            // (???) если в rotateX(yAngle), а в rotateY(xAngle) 
        return `translate(${x}px, ${y}px) rotateX(${xAngle}deg) rotateY(${yAngle}deg)  perspective(20px)`
    })

    const boxShadow = xy.interpolate((x, y) => {

        const halfWidth = window.innerWidth / 2
        const halfHeight = window.innerHeight / 2

        const offsetX = (x - halfWidth) * .1
        const offsetY = (y - halfHeight) * .1

        const distance = Math.sqrt(
            Math.pow(x - halfWidth, 2) + Math.pow(y - halfHeight, 2)
        )

        const maxDistance = Math.sqrt(halfWidth * halfWidth + halfHeight * halfHeight)

        const blur = 2 + distance * .1
        const opacity = .25 * ( 1 - distance / maxDistance) + .1

        return `${offsetX}px ${offsetY}px ${blur}px rgba(0, 0, 0, ${opacity})`
    })

    return (
        <Button {...bind()} style={{ transform, boxShadow }} />
    )
}

export default FlyingButton