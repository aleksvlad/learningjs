import React from 'react'
import FlyingButton from './FlyingButton'

function App() {
  return (
    <div className="main">
      <FlyingButton/>
    </div>
  );
}

export default App