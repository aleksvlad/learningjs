const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Loads env variables
require('dotenv').config()

// Initalizes express server
const app = express();
// specifies what port to run the server on
const PORT = process.env.PORT || 8000;

// Express middleware that can be used to enable CORS
app.use(cors())

// Adds json parsing middleware to incoming requests
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));


// Import Routes
const achievementsRoute = require('./routes/achievement');
const userRoute = require('./routes/user');

app.use('/achievements', achievementsRoute);
app.use('/user', userRoute);

//Routes
// app.get('/', (req, res) => {
// 	res.send('We are on HomePage')
// });

// Connect MongoDB
mongoose.connect(process.env.DB_CONNECTION, { useUnifiedTopology: true, useNewUrlParser: true }, () => {
    console.log("Connected to database")
});

app.listen(PORT, () => {
    console.log(`Running RESTful API on port http://localhost:${PORT}`);
});