const express = require('express');
const router = express.Router();

const Achievment = require('../models/Achievment');

// Create A post for specific user - Create
router.post('/:uid', async(req, res) => {
    const achievment = new Achievment({
        userId: req.params.uid,
        title: req.body.title,
        description: req.body.description,
        category: req.body.category,
        status: req.body.status,
        // img/video (?)
    })

    try {
        const savedAchievment = await achievment.save();
        res.json(savedAchievment)
    } catch (err) {
        res.json({ message: err })
    }
});

// Get All Posts for specific user - Read
router.get('/:uid', async(req, res) => {
    try {
        const achievments = await Achievment.find({ userId: { $eq: req.params.uid } });
        console.log(achievments)
        res.json(achievments);
    } catch (err) {
        res.json({ message: err });
    }
});

// Get Specific Post of user - Read
router.get('/:uid/:achievmentId', async(req, res) => {
    try {
        const achievment = await Achievment.findById(req.params.achievmentId);
        if (achievment.userId === req.params.uid) {
            res.json(achievment);
        } else {
            res.json({ message: "Incorrect User" });
        }
    } catch (err) {
        res.json({ message: err });
    }
});

// Update Specific Achievment of a user - Update
router.patch('/:uid/:achievmentId', async(req, res) => {
    try {
        const updatedAchievment = await Achievment.updateOne({ _id: req.params.achievmentId, userId: { $eq: req.params.uid } }, {
            $set: {
                title: req.body.title
            }
        });
        res.json(updatedAchievment);
    } catch (err) {
        res.json({ message: err });
    }
});

// Delete Specific Achievment of a user - Delete
router.delete('/:uid/:achievmentId', async(req, res) => {
    try {
        const removedAchievment = await Achievment.remove({ _id: req.params.achievmentId, userId: { $eq: req.params.uid } });
        res.json(removedAchievment);
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;