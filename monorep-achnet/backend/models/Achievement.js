import mongoose from 'mongoose';
const { Schema } = mongoose;

const AchivmentSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
        max: 1000
    },
    date: {
        type: Date,
        default: Date.now
    },
    category: {
        type: String,
        enum: ['спорт', 'путешествия', 'адреналин', 'природа', 'машины', 'мотоциклы', 'история', 'трэш', 'пранк', 'люди', 'музыка']
    },
    status: {
        type: String,
        enum: ['todo', 'progress', 'done']
    },
    images: {
        type: [Schema.Types.ObjectId],
        ref: 'Image'
    },
    video: {
        type: [Schema.Types.ObjectId],
        ref: 'Video'
    }
});

module.exports = mongoose.model('Achivment', AchivmentSchema);
export default AchivmentSchema;