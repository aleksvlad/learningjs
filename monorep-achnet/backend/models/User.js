import mongoose from 'mongoose';
const { Schema } = mongoose;

const UserSchema = new Schema({
    // id: {
    //     type: String,
    // },
    email: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    // phoneNumber: {
    //     type: String,
    //     required: true,
    //     min: 7,
    //     max: 15
    // },
    nickname: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    password: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    // bithdate: {
    // 	type: Date,
    // },
    registration_date: {
        type: Date,
        default: Date.now
    },
    achivments: {
        type: [Schema.Types.ObjectId],
        ref: 'Achivment'
    },
});

module.exports = mongoose.model('User', UserSchema);
export default UserSchema;