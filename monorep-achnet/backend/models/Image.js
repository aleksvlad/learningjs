import mongoose from 'mongoose';
const { Schema } = mongoose;

const ImagesSchema = new Schema({
    imgId: String,
    location: String,
    key: String,
    Bucket: String
});

module.exports = mongoose.model('Image', AchivmentSchema);
export default ImagesSchema;