import React, { useState }  from 'react';
import { View, TouchableOpacity } from 'react-native';
import Image from 'react-native-scalable-image';
import { Text } from '../../Themed';
import styles from './Styles';

export type Props = {
//   spacing?: Object;
};

const HeaderBtnPanel: React.FC<Props> = ({ }) => {

    // const [count, setCount] = useState(0);
    const onPress = () => console.log('search');
    const onBurgerBtnHandler = () => console.log('burger');
    const searchIconUri = 'https://storage.googleapis.com/vyzov-test-bucket/icons/search.png'

    return (
        <View style={[styles.btnPanelContainer]}>
            <TouchableOpacity
                style={styles.burgerBtn}
                onPress={onPress}
            >   
                <Image source={{ uri: searchIconUri }}/>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.burgerBtn}
                onPress={onBurgerBtnHandler}
            >
                <View style={styles.burgerIconLine} />
                <View style={styles.burgerIconLine}/>
                <View style={styles.burgerIconLine}/>
            </TouchableOpacity>
        </View>
    );
}

export default HeaderBtnPanel;