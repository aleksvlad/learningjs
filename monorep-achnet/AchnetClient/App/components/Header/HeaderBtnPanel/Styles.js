import { StyleSheet } from 'react-native';
import { Colors, Spacing } from "../../../styles";
import { scaleSize } from '../../../styles/mixins';

const styles = StyleSheet.create({
    btnPanelContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    burgerBtn: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: Spacing.SCALE_24,
        height: Spacing.SCALE_24,
        marginRight: Spacing.SCALE_16,
        paddingHorizontal: scaleSize(3),
        paddingVertical: Spacing.SCALE_6
    },
    burgerIconLine: {
        width: '100%',
        height: scaleSize(2),
        backgroundColor: Colors.BLACK
    }
});

export default styles 