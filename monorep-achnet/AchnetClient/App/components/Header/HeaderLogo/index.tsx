import React from 'react';
import { View } from 'react-native';
import Image from 'react-native-scalable-image';
import { Mixins } from '../../../styles';
import style from './Styles';

export type Props = {
  spacing?: Object;
};

const HeaderLogo: React.FC<Props> = ({ spacing }) => {

  const logoUri = 'https://storage.googleapis.com/vyzov-test-bucket/img/header-logo.png'

  return (
    <View style={[style.headerLogoContainer, spacing]}>
        <Image width={Mixins.scaleSize(108)} source={{ uri: logoUri }} />
    </View>
  );
}

export default HeaderLogo;