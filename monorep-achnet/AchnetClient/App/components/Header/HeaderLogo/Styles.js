import { StyleSheet } from 'react-native';
import { Spacing } from '../../../styles';

const styles = StyleSheet.create({
    headerLogoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: Spacing.SCALE_16,
        marginVertical: Spacing.SCALE_12
    },
});

export default styles 