import React from 'react';
import { View } from 'react-native';
import Image from 'react-native-scalable-image';
import HeaderBtnPanel from './HeaderBtnPanel';
// import style from './Styles';
import HeaderLogo from './HeaderLogo';
// import

export type Props = {
  activePage?: String;
};

const Header: React.FC<Props> = ({ activePage }) => {
    
  return (
    <>
        <HeaderLogo />
        <HeaderBtnPanel />
    </>
  );
}

export default Header;