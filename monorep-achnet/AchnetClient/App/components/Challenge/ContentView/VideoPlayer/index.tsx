import React from 'react';
import { useState, useEffect, useRef } from 'react';
import { ImageBackground, Image, TouchableWithoutFeedback, Button, View } from 'react-native';
import { Video } from 'expo-av';
import * as VideoThumbnails from 'expo-video-thumbnails';
import styles from './Styles'
import { scaleSize } from '../../../../styles/mixins';
// import { View } from '../../../Themed';

export type Props = {
  uri: string;
  isHaveBackground?: Boolean;
  isFocused?: boolean
};

export interface IVideoStatus {
  isPlaying: boolean;
}

// const Greeting: React.FC<Props> =

const ContentVideo: React.FC<Props> = ({ uri, isHaveBackground=false, isFocused=false }) => {
  const video: any = useRef();
  const [status, setStatus] = useState<IVideoStatus | undefined | any>(undefined);
  const [backgroundImage, setBackgroundImage] = useState<any>(null);
  console.log(uri)
//   const generateThumbnail = async () => {
//     try {
//       const { uri } = await VideoThumbnails.getThumbnailAsync(
//         'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
//         {
//           time: 1000,
//         }
//       );
//       console.log(uri)
//       setBackgroundImage(uri);
//     } catch (e) {
//       console.warn(e);
//     }
//   };
// 
//   useEffect(() => {
//     generateThumbnail();
//   },[]);
  
  return (
    <>
      {/* <View style={{width: 100, height: 20}}>
        <Button onPress={generateThumbnail} title="Generate thumbnail" />
      </View> */}
      {/* <ImageBackground source={backgroundImage} style={styles.backgroundImg}> */}
        <TouchableWithoutFeedback 
              style={[styles.container]}
              onPress={() => {
                  if (!status || !video) return;
                  status.isPlaying ? video.current.pauseAsync() : video.current.playAsync()
                }
              }
            >
                  {/* <Image source={{ uri: backgroundImage }}/> */}
                <View style={styles.videoWrapper}>
                  <Video
                      ref={video}
                      style={styles.video}
                      source={{
                        uri: uri,
                      }}
                      // shouldPlay
                      // useNativeControls
                      resizeMode="contain"
                      isLooping
                      onPlaybackStatusUpdate={status => setStatus(() => status)}
                    />
                </View>
              {/* </ImageBackground> */}
              {/* {backgroundImage !== null ? 
                <ImageBackground source={backgroundImage} style={styles.backgroundImg}>
                  <Video
                    ref={video}
                    style={styles.video}
                    source={{
                      uri: uri,
                    }}
                    // shouldPlay
                    // useNativeControls
                    resizeMode="contain"
                    isLooping
                    onPlaybackStatusUpdate={status => setStatus(() => status)}
                  />
                </ImageBackground> :
                <Video
                  ref={video}
                  style={styles.video}
                  source={{
                    uri: uri,
                  }}
                  // shouldPlay
                  // useNativeControls
                  resizeMode="contain"
                  isLooping
                  onPlaybackStatusUpdate={status => setStatus(() => status)}
                />
              } */}
        </TouchableWithoutFeedback>
      {/* </ImageBackground> */}
    </>
  );
}

export default ContentVideo;