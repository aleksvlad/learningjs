import { StyleSheet } from 'react-native';
import { Colors, Spacing } from '../../../../styles';
import { scaleSize } from '../../../../styles/mixins';
import { SCALE_10 } from '../../../../styles/spacing';

const styles = StyleSheet.create({
    container: {
      width: scaleSize(255),
      height: scaleSize(455),
      flex: 1,
      backgroundColor: Colors.WHITE,
      marginHorizontal: Spacing.SCALE_8
    },
    videoWrapper: {
      backgroundColor: 'black', 
      width: scaleSize(255), 
      height: scaleSize(455), 
      marginHorizontal: Spacing.SCALE_8, 
      marginVertical: Spacing.SCALE_8, 
      flex: 1, 
      alignContent: 'center', 
      justifyContent: 'center',
      borderBottomLeftRadius: 6,
      borderBottomRightRadius: 6,
      borderTopRightRadius: 6,
      borderTopLeftRadius: 6,
    },
    video: {
      alignSelf: 'center',
      width: scaleSize(255),
      height: scaleSize(455),
      borderBottomLeftRadius: 6,
      borderBottomRightRadius: 6,
      borderTopRightRadius: 6,
      borderTopLeftRadius: 6,
    },
    backgroundImg: {
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center'
    }
});

export default styles;