import React from 'react';
import { useState, useEffect, useRef } from 'react';
import { FlatList, ScrollView } from 'react-native';
import { View } from '../Themed';
import ContentViewList from './ContentViewList';
import ControlPanel from './ControlPanel';
import Description from './Description';
import styles from './Styles'
// import { View } from '../../../Themed';

export type Props = {
  data: any;
  isFocused?: Boolean;
};

const Challenge: React.FC<Props> = ({ data, isFocused=false }) => {  
    console.log(data)

    const keyExtractor = ({ id }:any) => id;
    // const avatarExample = 'https://storage.googleapis.com/vyzov-test-bucket/icons/anonymous-avatar.png'

    return (
        <View style={styles.challengeContainer}>
            <Description 
                creatorId={121}
                creatorName={'John69'}
                creatorAvatar={data.avatarURL}
                descriptionText={data.descriptionText}
            />
            <ContentViewList data={data.chalengeContent} isFocused={isFocused} />
            <ControlPanel />
        </View>
    );
}

export default Challenge ;