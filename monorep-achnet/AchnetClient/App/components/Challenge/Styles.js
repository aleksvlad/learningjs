import { StyleSheet } from 'react-native';
import { Spacing } from '../../styles';

const styles = StyleSheet.create({
    challengeContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
        paddingVertical: Spacing.SCALE_4,
        paddingLeft: Spacing.SCALE_20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,
        elevation: 12,
    },
});

export default styles 