import { StyleSheet, Dimensions } from 'react-native';
import { Colors, Spacing, Typography } from '../../../styles';
import { scaleSize } from '../../../styles/mixins';

const styles = StyleSheet.create({
    controlPanelContainer: {
        width: Dimensions.get('window').width - Spacing.SCALE_20,
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: Spacing.SCALE_8,
        paddingBottom: Spacing.SCALE_8
    },
    controlPanelBtnsWrapper: {
        display: 'flex',
        flexDirection: 'row'
    },
    controlPanelBtnWrapper: {
        width: Spacing.SCALE_48,
        // display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
        // 
    },
    controlPanelBtn: {
        width: Spacing.SCALE_24,
        height: Spacing.SCALE_24,
        flex: 1,
        display: 'flex',
        resizeMode: 'contain',
        alignItems: 'center',
        justifyContent: 'center'
    },
    iWillDoBtn: {
        width: Spacing.SCALE_22,
        height: Spacing.SCALE_22,
        backgroundColor: Colors.BLACK,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Spacing.SCALE_4                
    },
    checkmark: {
        height: Spacing.SCALE_6,
        width: scaleSize(13.5),
        // borderColor: Colors.WHITE,
        borderWidth: scaleSize(1.5),
        // borderBottomWidth : scaleSize(1.5),
        // borderLeftWidth: scaleSize(1.5),
        transform: [{rotate: '-60deg'}]
    },
    counterText: {
        color: Colors.BLACK,
        fontFamily: Typography.FONT_FAMILY_LIGHT,
        fontWeight: Typography.FONT_WEIGHT_LIGHT,
        fontSize: Typography.FONT_SIZE_10,
        lineHeight: Typography.LINE_HEIGHT_12
    },
});

export default styles 