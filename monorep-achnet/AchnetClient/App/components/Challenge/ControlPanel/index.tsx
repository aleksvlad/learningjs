import React, { useState, useEffect, useRef } from 'react';
import { View, Text } from '../../Themed';
import { TouchableOpacity } from 'react-native';
import Image from 'react-native-scalable-image';
import styles from './Styles'
import { Mixins, Spacing } from '../../../styles';

export type Props = {
//   data: any;
//   isFocused?: Boolean;
};

const ControlPanel: React.FC<Props> = ({}) => {  
    const [willDoCounter, setWillDoCounter] = useState(0)
    const [comentsCounter, setComentsCounter] = useState(0)
    const [repostCounter, setRepostCounter] = useState(0)
    const [activeVideo, setActiveVideo] = useState(1)
    const [postVideosCounter, setPostVideosCounter] = useState(3)

    const commentUrl = 'https://storage.googleapis.com/vyzov-test-bucket/icons/comments-icon.png'
    const repostIconUrl = 'https://storage.googleapis.com/vyzov-test-bucket/icons/reply-icon.png'

    return (
        <View style={styles.controlPanelContainer}>
            <View style={styles.controlPanelBtnsWrapper}>
                <View style={styles.controlPanelBtnWrapper}>
                    <Text>{willDoCounter}</Text>          
                    <TouchableOpacity
                        style={styles.controlPanelBtn}
                    >
                        <View style={styles.iWillDoBtn}>
                            <View style={styles.checkmark}/>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.controlPanelBtnWrapper}>
                    <Text>{comentsCounter}</Text>          
                    <TouchableOpacity
                        style={styles.controlPanelBtn}>
                        <Image source={{uri: commentUrl}} width={24} />                    
                    </TouchableOpacity>
                </View>
                <View style={styles.controlPanelBtnWrapper}>
                    <Text>{repostCounter}</Text>
                    <TouchableOpacity
                        style={styles.controlPanelBtn}>
                        <Image source={{uri: repostIconUrl}} style={{    flex: 1,
                            width: 20,
                            height: 20,
                            resizeMode: 'contain' }} width={24} />
                    </TouchableOpacity>          
                </View>
            </View>
            <Text>{activeVideo}/{postVideosCounter}</Text>
        </View>
    );
}

export default ControlPanel;