import React from 'react';
import { useState, useEffect, useRef } from 'react';
import { FlatList, ScrollView } from 'react-native';
import ContentVideo from '../ContentView/VideoPlayer';
import styles from './Styles'
// import { View } from '../../../Themed';

export type Props = {
  data: any;
  isFocused?: Boolean;
};

const ContentViewList: React.FC<Props> = ({ data, isFocused=false }) => {  
    console.log(data)
    const keyExtractor = ({ id }:any) => id;

    const renderItem = ({ item }: any) => (
        <ContentVideo uri={item.uri} isHaveBackground={item.isHaveBackground} isFocused={item.isFocused}/>
    )

    return (
        <ScrollView
            showsHorizontalScrollIndicator={false} 
            horizontal={true}>
            <FlatList
                horizontal
                keyExtractor={keyExtractor} 
                data={data}
                renderItem={renderItem}
            />
        </ScrollView>
    );
}

export default ContentViewList;