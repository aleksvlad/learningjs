import { StyleSheet } from 'react-native';
import { Colors } from '../../../../styles';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignContent: 'flex-start',
      backgroundColor: Colors.WHITE,
    },
});

export default styles;