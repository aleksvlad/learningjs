import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import Image from 'react-native-scalable-image';
import { Mixins, Spacing } from '../../../styles';
import styles from './Styles';

interface IChallengeDescription {
    creatorName: string;
    creatorId: number,
    creatorAvatar: string,
    descriptionText: string
};
  
const ChallengeDescription: React.FC<IChallengeDescription> = ({ creatorId, creatorName, creatorAvatar, descriptionText }) => {
  // РЕШИТЬ ПРОБЛЕМУ С ПЕРЕРЕНДЕРОМ ВСЕГО СПИСКА!!!
  const onMoreClickHandler = () => {
    console.log('more btn')
  }
  return (
    <View style={styles.descriptionContainer}>
      {/* <TouchableOpacity
        onPress={onPress}
        activeOpacity={activeOpacity}
      >
      </TouchableOpacity> */}
        {/* <View style={styles.creatorAvatar}> */}
        <Image source={{ uri: creatorAvatar }} width={Spacing.SCALE_50} style={styles.creatorAvatar}/>
        {/* </View> */}
        <View
          style={styles.textContainer}
        >
          <Text
            numberOfLines={1}
            style={styles.creatorNickname}
          >
            {creatorName}
          </Text>
          <Text style={styles.descriptionText}>
              {descriptionText}
          </Text>
        </View>
        <TouchableOpacity
            style={styles.moreBtn}
            onPress={onMoreClickHandler}
            activeOpacity={0.3}
        >
            <View style={styles.moreIconDot} />
            <View style={styles.moreIconDot} />
            <View style={styles.moreIconDot} />
        </TouchableOpacity>
    </View>
  );
}

export default React.memo(ChallengeDescription);