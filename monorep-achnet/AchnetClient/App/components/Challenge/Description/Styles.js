import { StyleSheet, Dimensions } from 'react-native';
import { Colors, Spacing, Typography } from "../../../styles";
import { scaleSize } from '../../../styles/mixins';

const styles = StyleSheet.create({
    descriptionContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        width: Dimensions.get('window').width - Spacing.SCALE_20,
        marginVertical: Spacing.SCALE_8
    },
    textContainer: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginLeft: Spacing.SCALE_8,
        paddingTop: Spacing.SCALE_4
        // paddingHorizontal: scaleSize(3),
        // paddingVertical: Spacing.SCALE_6
    },
    creatorNickname: {
        color: Colors.GRAY_TEXT,
        fontSize: Typography.FONT_SIZE_14,
        lineHeight: Typography.LINE_HEIGHT_16,
        fontFamily: Typography.FONT_FAMILY_BOLD,
        paddingBottom: Spacing.SCALE_4
    },
    creatorAvatar: {
        width: Spacing.SCALE_50,
        height: Spacing.SCALE_50,
        borderRadius: Spacing.SCALE_50 / 2
    },
    descriptionText: {
        color: Colors.BLACK,
        fontSize: Typography.FONT_SIZE_12,
        lineHeight: Typography.LINE_HEIGHT_14,
        fontFamily: Typography.FONT_FAMILY_REGULAR,
        textAlign: 'left'
    },
    moreBtn: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Spacing.SCALE_24,
        height: Spacing.SCALE_24,
        paddingHorizontal: Spacing.SCALE_4,
        paddingVertical: Spacing.SCALE_10,
        position: 'absolute',
        right: Spacing.SCALE_8,
        top: Spacing.SCALE_4
    },
    moreIconDot: {
        width: scaleSize(4),
        height: scaleSize(4),
        backgroundColor: Colors.GRAY_ICON,
        borderRadius: scaleSize(2)
    }
});

export default styles 