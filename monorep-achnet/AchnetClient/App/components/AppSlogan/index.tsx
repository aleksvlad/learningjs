import React from 'react';
import { View, Text } from 'react-native';
import styles from './Styles';
import { globalStyles } from '../../styles/index';

export type Props = {
  spacing?: Object;
};

const AppSlogan: React.FC<Props> = ({ spacing }) => {
  return (
    <View style={[styles.appSloganContainer, spacing]}>
        <Text
          style={[styles.appSloganText, globalStyles.title]}>
          Выполняй новые для себя челенджи и
          делись об этом со своим окружением
        </Text>
    </View>
  );
}

export default AppSlogan