import { StyleSheet } from "react-native";
import { Spacing, Typography } from "../../styles";

const styles = StyleSheet.create({
    appSloganContainer: {
      alignItems: 'center',
      marginHorizontal: Spacing.SCALE_12,
    },
    appSloganText: {
      fontSize: Typography.FONT_SIZE_18,
      lineHeight: Spacing.SCALE_40,
      textAlign: 'center',
      paddingTop: Spacing.SCALE_4
    },
});

export default styles