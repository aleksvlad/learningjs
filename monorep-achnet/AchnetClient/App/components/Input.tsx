import React, { Component } from 'react';
import { TextInput } from 'react-native';
import { globalStyles } from '../styles';

export type Props = {
    value?: string,
    onChangeText?: any,
    style?: Object,
};  

const Input: React.FC<Props> = ({ value, onChangeText, style = {}}) => {
  return (
    <TextInput
      style={[globalStyles.input, style]}
      onChangeText={onChangeText}
      value={value}
    />
  );
};

export default Input;