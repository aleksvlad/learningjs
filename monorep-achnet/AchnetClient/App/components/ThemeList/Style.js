import { StyleSheet } from 'react-native';
import { Spacing } from '../../styles';

const styles = StyleSheet.create({
    tagsContainer: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: Spacing.SCALE_24
    }
})

export default styles