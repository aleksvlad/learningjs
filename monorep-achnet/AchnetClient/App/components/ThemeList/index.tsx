import React, { useState } from 'react'
import { useEffect } from 'react'
import {
    View,
} from 'react-native'
import styles from './Style'
import ThemeTag from './ThemeTag'

interface Props {
    value: Array<Object>,
    data: Array<Object>,
    max: number, 
    onMaxError: any,
    onItemPress: any
    labelAttr?: string,
    keyAttr?: string,
};
  
const ThemeList: React.FC<Props> = ({ value, data, max, onMaxError, onItemPress, labelAttr='label', keyAttr='id' }) => {

  // data.map((item: any) => {
  //   console.log(item[labelAttr])
  //   console.log(item[keyAttr])
  // })
  
  const [selectedValues, setSelectedValues]: any = useState(value)
    
  /**
      * @description Return the number of items selected
      * @return {Number}
  */
  const totalSelected = () => {
    return Object.keys(selectedValues).length
  }

  /**
    * @description Return the items selected
    * @return {Array}
  */
  const itemsSelected = () => {
    const items: any[] = []
    
    Object.entries(selectedValues).forEach(([key]) => {
      items.push(selectedValues[key])
    })
    
    return items
  }

    // /**
    //  * @description Callback after select an item
    //  * @param {Object} item
    //  * @return {Void}
    // */
    const handleSelectItem = (id: any) => {
      if (selectedValues.includes(id)) {
        setSelectedValues(selectedValues.filter((value: any) => value !== id))
      } else {
        setSelectedValues([...selectedValues, id])
      }
    //   const key: string = item[keyAttr] || item
    // 
    //   const value = { ...selectedValues }
    //   const found = selectedValues[key]
    // 
    //   // Item is on array, so user is removing the selection
    //   if (found) {
    //     delete value[key]
    //   } else {
    //       // User is adding but has reached the max number permitted
    //     if (max && totalSelected() >= max) {
    //       if (onMaxError) {
    //         return onMaxError()
    //       }
    //     }
    //     value[key] = item
    //   }
    // 
    //   return setSelectedValues({ value }, () => {
    //     if (onItemPress) {
    //       onItemPress(item)
    //     }
    //   })
    }

    // useEffect(()=>{
    //   console.log(selectedValues)
    // },[selectedValues])
    
    return(
      <View
        style={[
          styles.tagsContainer
        ]}
      >
        {data.map((item: any) => 
           <ThemeTag
            label={item[labelAttr]}
            key={item[keyAttr] ? item[keyAttr] : null}
            onPress={() => handleSelectItem(item[keyAttr])}
            selected={selectedValues.includes(item[keyAttr])}
            activeOpacity={0}
          />
        )}
      </View>
  );
}

export default ThemeList