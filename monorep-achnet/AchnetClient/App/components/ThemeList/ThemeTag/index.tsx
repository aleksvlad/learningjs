import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import styles from './Style';

interface IThemeTag {
    label: string;
    onPress: any,
    selected: boolean,
    activeOpacity: number
};
  
const ThemeTag: React.FC<IThemeTag> = ({ label, onPress, selected, activeOpacity }) => {
  // РЕШИТЬ ПРОБЛЕМУ С ПЕРЕРЕНДЕРОМ ВСЕГО СПИСКА!!!
  console.log(label)
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={activeOpacity}
      >
        <View
          style={[
            styles.inner,
            styles.defaultInner,
            selected && styles.defaultInnerSelected,
          ]}
        >
          <Text
            numberOfLines={1}
            style={[
              styles.defaultLabelText,
              selected && styles.defaultLabelTextSelected
            ]}
          >
            {label}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default React.memo(ThemeTag);