import { StyleSheet } from 'react-native';
import { Colors } from '../../../styles';

const styles = StyleSheet.create({
    container: {
      marginBottom: 10,
      marginRight: 10
    },
    inner: {
      padding: 10,
      borderWidth: 1,
      borderRadius: 6
    },
    defaultInner: {
      backgroundColor: '#f8f9fa',
      borderColor: '#f8f9fa'
    },
    defaultInnerSelected: {
      backgroundColor: Colors.BLACK,
      borderColor: Colors.GRAY_TEXT
    },
    defaultLabelText: {
      color: Colors.BLACK
    },
    defaultLabelTextSelected: {
      color: Colors.WHITE
    },
    inverseInner: {
      backgroundColor: '#FFFFFF',
      borderColor: '#343a40'
    },
    inverseInnerSelected: {
      backgroundColor: '#343a40',
      borderColor: '#343a40'
    },
    inverseLabelText: {
      color: Colors.BLACK
    },
    inverseLabelTextSelected: {
      color: Colors.WHITE
    },
    successInner: {
      backgroundColor: '#FFFFFF',
      borderColor: '#28a745'
    },
    successInnerSelected: {
      backgroundColor: '#28a745',
      borderColor: '#28a745'
    },
    successLabelText: {
      color: '#28a745'
    },
    successLabelTextSelected: {
      color: '#FFF'
    },
    infoInner: {
      backgroundColor: '#FFFFFF',
      borderColor: '#007BFF'
    },
    infoInnerSelected: {
      backgroundColor: '#007bff',
      borderColor: '#007BFE'
    },
    infoLabelText: {
      color: '#004085'
    },
    infoLabelTextSelected: {
      color: '#FFF'
    },
    warningInner: {
      backgroundColor: '#FFFFFF',
      borderColor: '#ffc107'
    },
    warningInnerSelected: {
      backgroundColor: '#ffc107',
      borderColor: '#ffc107'
    },
    warningLabelText: {
      color: '#333'
    },
    warningLabelTextSelected: {
      color: '#333'
    },
    dangerInner: {
      backgroundColor: '#FFFFFF',
      borderColor: '#dc3545'
    },
    dangerInnerSelected: {
      backgroundColor: '#dc3545',
      borderColor: '#dc3545'
    },
    dangerLabelText: {
      color: '#dc3545'
    },
    dangerLabelTextSelected: {
      color: '#FFF'
    }
})

export default styles