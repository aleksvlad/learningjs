import React, { useState } from "react";
import { Text, TouchableOpacity } from "react-native";
import { globalStyles } from "../styles";

export type Props = {
    text: string;
    onPress: any;
    styles?: Object,
};
  

const Button: React.FC<Props> = ({ text, onPress, styles = {} }) => {
    return (
        <TouchableOpacity
            style={[globalStyles.button, styles]}
            onPress={onPress}
        >
            <Text style={globalStyles.buttonText}>{text}</Text>
        </TouchableOpacity>
    )
}

export default Button