import React from 'react';
import { View } from 'react-native';
import Image from 'react-native-scalable-image';
import style from './Styles';

export type Props = {
  spacing?: Object;
};

const OpeningLogo: React.FC<Props> = ({ spacing }) => {

  const logoUri = 'https://storage.googleapis.com/vyzov-test-bucket/img/opening-logo.png'

  return (
    <View style={[style, spacing]}>
        <Image width={100} source={{ uri: logoUri }} />
    </View>
  );
}

export default OpeningLogo;