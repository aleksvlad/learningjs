import { StyleSheet } from 'react-native';
import { Spacing } from "../../styles";

const styles = StyleSheet.create({
    openingLogoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: Spacing.SCALE_10,
    },
    openingLogo: {
        marginHorizontal: 0,
        marginTop: Spacing.SCALE_40,
        marginBottom: Spacing.SCALE_36
    },
});

export default styles 