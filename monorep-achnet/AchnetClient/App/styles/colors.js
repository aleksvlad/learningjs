export const WHITE = '#FFFFFF';
export const BLACK = '#000000';

export const GRAY_TEXT = '#656565';
export const GRAY_ICON = '#666666';
export const INPUT_TEXT = '#999999';