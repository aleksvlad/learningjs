import  { scaleSize }  from './mixins';

export const SCALE_50 = scaleSize(50);
export const SCALE_48 = scaleSize(48);
export const SCALE_40 = scaleSize(40);
export const SCALE_38 = scaleSize(38);
export const SCALE_36 = scaleSize(36);
export const SCALE_32 = scaleSize(32);
export const SCALE_24 = scaleSize(24);
export const SCALE_22 = scaleSize(22);
export const SCALE_20 = scaleSize(20);
export const SCALE_18 = scaleSize(18);
export const SCALE_16 = scaleSize(16);
export const SCALE_12 = scaleSize(12);
export const SCALE_10 = scaleSize(10);
export const SCALE_8 = scaleSize(8);
export const SCALE_6 = scaleSize(6);
export const SCALE_5 = scaleSize(5);
export const SCALE_4 = scaleSize(4);