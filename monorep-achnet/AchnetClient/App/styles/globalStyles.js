import { Colors, Spacing, Typography } from '.';
import  { scaleSize }  from './mixins';
import { StyleSheet } from 'react-native';

export const globalStyles = StyleSheet.create({    
    authPagesWrapper: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around'
    },
    pageWrapper: {
      flex: 1,
      backgroundColor: Colors.WHITE
    },
    button: {
      backgroundColor: Colors.BLACK,
      color: Colors.WHITE,
      textAlign: "center",
      borderRadius: 6,
      width: scaleSize(150),
      paddingVertical: Spacing.SCALE_5
    },
    buttonText: {
      fontFamily: Typography.FONT_FAMILY_REGULAR,
      fontWeight: Typography.FONT_WEIGHT_REGULAR,
      fontSize: Typography.FONT_SIZE_12,
      lineHeight: Typography.LINE_HEIGHT_14,
      textAlign: 'center',
      color: Colors.WHITE
    },
    input: {
      backgroundColor: Colors.WHITE,
      borderWidth: 0,
      borderRadius: 4,
      width: scaleSize(154),
      height: scaleSize(24),
      paddingVertical: Spacing.SCALE_5,
      paddingHorizontal: Spacing.SCALE_8,
      marginVertical: Spacing.SCALE_12,
      color: Colors.INPUT_TEXT,
      fontFamily: Typography.FONT_FAMILY_REGULAR,
      fontWeight: Typography.FONT_WEIGHT_REGULAR,
      fontSize: Typography.FONT_SIZE_12,
      lineHeight: Typography.LINE_HEIGHT_14,
      shadowColor: '#000',
      shadowOpacity: 0.35,
      shadowOffset: { width: 0, height: 0 },
      shadowRadius: 8,  
      elevation: 5
    },
    title: {
      fontFamily: Typography.FONT_FAMILY_REGULAR,
      fontWeight: Typography.FONT_WEIGHT_REGULAR,
      fontSize: Typography.FONT_SIZE_16,
      lineHeight: Typography.LINE_HEIGHT_24,
      color: Colors.BLACK
    },
    text: {
      fontFamily: Typography.FONT_FAMILY_REGULAR,
      fontWeight: Typography.FONT_WEIGHT_REGULAR,
      fontSize: Typography.FONT_SIZE_12,
      lineHeight: Typography.LINE_HEIGHT_14,
      color: Colors.BLACK
    },
    modalContainer: {
      paddingVertical: Spacing.SCALE_36,
      paddingHorizontal: Spacing.SCALE_38,
      backgroundColor: Colors.WHITE,
      borderRadius: 8,
      shadowColor: '#000',
      shadowOpacity: 0.25,
      shadowOffset: { width: 2, height: 4 },
      shadowRadius: 4,  
      elevation: 5
    },
    separator: {
      marginTop: Spacing.SCALE_18,
      height: 1,
      width: '80%',
    },
});