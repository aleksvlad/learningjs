import { scaleFont } from './mixins';

// FONT FAMILY
export const FONT_FAMILY_LIGHT = 'roboto-light';
export const FONT_FAMILY_REGULAR = 'roboto-regular';
export const FONT_FAMILY_MEDIUM = 'roboto-medium';
export const FONT_FAMILY_BOLD = 'roboto-bold';

// FONT WEIGHT
export const FONT_WEIGHT_LIGHT = '300';
export const FONT_WEIGHT_REGULAR = '400';
export const FONT_WEIGHT_MEDIUM = '500';
export const FONT_WEIGHT_BOLD = '700';

// FONT SIZE
export const FONT_SIZE_18 = scaleFont(18);
export const FONT_SIZE_16 = scaleFont(16);
export const FONT_SIZE_14 = scaleFont(14);
export const FONT_SIZE_12 = scaleFont(12);
export const FONT_SIZE_10 = scaleFont(10);

// LINE HEIGHT
export const LINE_HEIGHT_24 = scaleFont(24);
export const LINE_HEIGHT_20 = scaleFont(20);
export const LINE_HEIGHT_16 = scaleFont(16);
export const LINE_HEIGHT_14 = scaleFont(14);
export const LINE_HEIGHT_12 = scaleFont(12);

// FONT STYLE
export const FONT_LIGHT = {
    fontFamily: FONT_FAMILY_REGULAR,
    fontWeight: FONT_WEIGHT_REGULAR,
}

export const FONT_REGULAR = {
    fontFamily: FONT_FAMILY_LIGHT,
    fontWeight: FONT_WEIGHT_LIGHT,
};

export const FONT_MEDIUM = {
    fontFamily: FONT_FAMILY_REGULAR,
    fontWeight: FONT_WEIGHT_REGULAR,
}
  
export const FONT_BOLD = {
    fontFamily: FONT_FAMILY_BOLD,
    fontWeight: FONT_WEIGHT_BOLD,
};