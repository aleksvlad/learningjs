import React, { useState, useEffect } from 'react';
import { Text, View } from '../../components/Themed';
import { TextInput, Animated, Keyboard, KeyboardAvoidingView } from 'react-native';
import AppSlogan from '../../components/AppSlogan';
import OpeningLogo from '../../components/OpeningLogo';
import Input from '../../components/Input';
import Button from '../../components/Button';
import styles from './Styles';
import { globalStyles, Spacing } from '../../styles';

export type Props = {
  navigation: any;
};

const SignUp: React.FC<Props> = ({ navigation }) => {

  const [login, onChangeLogin] = React.useState('Логин');
  const [password, onChangePassword] = React.useState('');
  const [passwordRepeat, onChangePasswordRepeat] = React.useState('');

  return (
    <KeyboardAvoidingView 
      style={styles.container}
      // behavior="padding"
    >
      <OpeningLogo spacing={styles.logo}/>
      <AppSlogan spacing={styles.title}/>
      <View style={styles.formContainer}>
        <Input
          value={login}
          style={{borderColor: 'gray', borderWidth: 1}}
          onChangeText={(login: string) => onChangeLogin(login)}
        />
        <TextInput
          style={[globalStyles.input, { borderColor: 'gray', borderWidth: 1 }]}
          onChangeText={(password: string) => onChangePassword(password)}
          secureTextEntry={true}
          placeholder={'Пароль'}
          value={password}
        />
        <TextInput
          style={[globalStyles.input, { borderColor: 'gray', borderWidth: 1 }]}
          onChangeText={(passwordRepeat: string) => onChangePassword(passwordRepeat)}
          secureTextEntry={true}
          placeholder={'Повторите пароль'}
          value={passwordRepeat}
        />
        <Button
          styles={styles.btn}
          onPress={() => {
            console.log('dispatch for reg')
            navigation.navigate('Interests')
          }}
          text="Регистрация"  
        />        
      </View>
      <View style={[globalStyles.separator, { marginTop: Spacing.SCALE_12 }]} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />      
      <Text 
        style={styles.linkToSignIn}
        onPress={() => navigation.navigate('Login')}  
      >
        Уже зарегистрирован
      </Text>
    </KeyboardAvoidingView>
  );
}

export default SignUp