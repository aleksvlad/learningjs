import { StyleSheet } from 'react-native';
import { Spacing, Colors, Mixins } from '../../styles';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE,
        paddingBottom: Mixins.scaleSize(110),
        paddingTop: Spacing.SCALE_40
    },
    formContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        margin: 0
        // marginBottom: Spacing.SCALE_24
    },
    title: {
        marginBottom: Spacing.SCALE_40
    },
    logo: {
        marginVertical: Spacing.SCALE_40
    },
    btn: {
        marginVertical: Spacing.SCALE_12
    },
    separator: {
        marginVertical: Spacing.SCALE_24,
        height: 1,
        width: '80%',
    },
    linkToSignIn: {
        paddingTop: Spacing.SCALE_24
    },
});

export default styles