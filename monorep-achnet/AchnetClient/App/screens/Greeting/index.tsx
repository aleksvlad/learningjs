import * as React from 'react';
import { View } from 'react-native';
import Button from '../../components/Button';
import AppSlogan from '../../components/AppSlogan';
import OpeningLogo from '../../components/OpeningLogo';
import styles from './Styles';

export type Props = {
  navigation: any;
};

const Greeting: React.FC<Props> = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <OpeningLogo spacing={styles.logo}/>
      <AppSlogan spacing={styles.title}/>
      <Button
        styles={styles.btn}
        onPress={() => navigation.navigate('SignUp')}
        text="Регистрация"  
      />
      <Button
        styles={styles.btn}
        onPress={() => navigation.navigate('Login')}
        text="Войти"  
      />
    </View>
  );
}

export default Greeting;