import { StyleSheet } from 'react-native';
import { Colors, Mixins, Spacing } from '../../styles';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors.WHITE,
      paddingVertical: Spacing.SCALE_40
    },
    title: {
        paddingTop: 0,
        marginBottom: Mixins.scaleSize(110)
    },
    logo: {
        marginVertical: Spacing.SCALE_40
    },
    btn: {
        marginBottom: Spacing.SCALE_12
    },
});

export default styles;