import React, { useState, useEffect } from 'react';
import AppSlogan from '../../components/AppSlogan';
import OpeningLogo from '../../components/OpeningLogo';
import { Text, View } from '../../components/Themed';
import { TextInput, TouchableOpacity } from 'react-native';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Image from 'react-native-scalable-image';
// import {
//   GoogleSignin,
//   GoogleSigninButton,
//   statusCodes,
// } from '@react-native-google-signin/google-signin';
import styles from './Styles';
import { globalStyles, Spacing } from '../../styles';

export type Props = {
  navigation: any;
};

const Login: React.FC<Props> = ({ navigation }) => {

  const [login, onChangeLogin] = useState('Логин');
  const [password, onChangePassword] = useState('');
  const [loggedIn, setloggedIn] = useState(false);
  const [userInfo, setUserInfo] = useState([]);

  return (
    <View style={styles.container}>
      <OpeningLogo spacing={styles.logo}/>
      <AppSlogan spacing={styles.title}/>
      <View style={styles.formContainer}>
        <Input
          value={login}
          style={{borderColor: 'gray', borderWidth: 1}}
          onChangeText={(login: string) => onChangeLogin(login)}
        />
        <TextInput
          style={[globalStyles.input, { borderColor: 'gray', borderWidth: 1 }]}
          onChangeText={(password: string) => onChangePassword(password)}
          secureTextEntry={true}
          placeholder={'Пароль'}
          value={password}
        />
        <TouchableOpacity
            style={styles.googleSignIn}
            activeOpacity={0.5}>
          <Image style={{margin: 0, padding: 0}} width={24} source={{ uri: 'https://storage.googleapis.com/vyzov-test-bucket/img/google-icon.png'}} />
          {/* <Text style={styles.buttonTextStyle}>
              Sign in with Google
          </Text> */}
        </TouchableOpacity>
        {/* <GoogleSigninButton
          style={{width: 192, height: 48}}
          size={GoogleSigninButton.Size.Wide}}
          color={GoogleSigninButton.Color.Dark}
          onPress={() => { console.log('sign in with google')}}
          // signIn
        /> */}
        <Button
          styles={styles.btn}
          onPress={() => console.log('dispatch for log')}
          text="Войти"  
        />
      </View>
      
      <View style={[globalStyles.separator, { marginTop: Spacing.SCALE_24 }]} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />      
      <Text 
        style={styles.linkToSignUp}
        onPress={() => navigation.navigate('SignUp')}
      >
        Перейти к регистрации
      </Text>
    </View>
  );
}

export default Login