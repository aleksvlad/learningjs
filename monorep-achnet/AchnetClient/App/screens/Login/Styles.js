import { StyleSheet } from 'react-native';
import { Spacing, Colors, Mixins, Typography } from '../../styles';
import { scaleSize } from '../../styles/mixins';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE,
        paddingBottom: Mixins.scaleSize(110),
        paddingTop: Spacing.SCALE_40
    },
    formContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginTop: 0
        // marginBottom: Spacing.SCALE_24
    },
    title: {
        marginBottom: Spacing.SCALE_40,
    },
    linkToSignUp: {
        paddingTop: Spacing.SCALE_24
    },
    logo: {
        marginVertical: Spacing.SCALE_40
    },
    btn: {
        marginVertical: Spacing.SCALE_12
    },
    googleSignIn: {
        width: scaleSize(35),
        height: scaleSize(35),
        borderRadius: 17, 
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginVertical: Spacing.SCALE_16,
        paddingVertical: Spacing.SCALE_16
    },
    buttonTextStyle: {
        fontFamily: Typography.FONT_FAMILY_REGULAR,
        fontWeight: Typography.FONT_WEIGHT_REGULAR,
        fontSize: Typography.FONT_SIZE_12,
        lineHeight: Typography.LINE_HEIGHT_14,
        textAlign: 'center',
        color: Colors.BLACK,
        paddingLeft: Spacing.SCALE_12
    }
});

export default styles