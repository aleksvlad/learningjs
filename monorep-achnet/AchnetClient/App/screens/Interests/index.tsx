import * as React from 'react';
import { View, Button, Text, Alert } from 'react-native';
import AppSlogan from '../../components/AppSlogan';
import OpeningLogo from '../../components/OpeningLogo';
import styles from './Styles';
import ThemeTag from '../../components/ThemeList/ThemeTag';
import ThemeList from '../../components/ThemeList';
import { Spacing } from '../../styles';

const data = [
  { id: 1, label: 'Сноуборд' },
  { id: 2, label: 'Музыка' },
  { id: 3, label: 'Кулинария' },
  { id: 4, label: 'Бег' },
  { id: 5, label: 'BMX' },
  { id: 8, label: 'Видеоигры' },
  { id: 6, label: 'Искусство' },
  { id: 7, label: 'Общение' }
];

export type Props = {
  navigation: any;
};

const Interests: React.FC<Props> = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <OpeningLogo spacing={styles.logo}/>
      <AppSlogan spacing={styles.title} />
      <ThemeList
        value={[data[0].id]}
        data={data}
        max={5}
        onItemPress={() => {
          console.log('press')
        }}
        onMaxError={() => {
          Alert.alert('Ops', 'Max reached');
        }}
      />
      <Text 
        style={styles.linkToFeed}
        onPress={() => navigation.navigate('Feed')}
      >
        Подтвердить
      </Text>
    </View>
  );
}

export default Interests