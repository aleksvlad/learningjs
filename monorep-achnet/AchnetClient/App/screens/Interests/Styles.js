import { StyleSheet } from 'react-native';
import { Colors, Mixins, Spacing } from '../../styles';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors.WHITE,
      paddingVertical: Spacing.SCALE_40
    },
    title: {
        paddingTop: 0,
        marginBottom: Spacing.SCALE_48
    },
    logo: {
        marginBottom: Spacing.SCALE_40
    },
    btn: {
        marginBottom: Spacing.SCALE_12
    },
    item: {
        borderWidth: 1,
        borderColor: '#333',    
        backgroundColor: '#FFF',
    },
    label: {
        color: '#333'
    },
    itemSelected: {
        backgroundColor: '#333',
    },
    labelSelected: {
        color: '#FFF',
    },
    linkToFeed: {
        paddingTop: Spacing.SCALE_16
    },
});

export default styles; 