import { StyleSheet } from 'react-native';
import { Colors, Mixins, Spacing } from '../../styles';
import { scaleSize } from '../../styles/mixins';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'flex-start',
      backgroundColor: Colors.WHITE
    },
    addPostBtn: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: scaleSize(5),
      width: Mixins.scaleSize(50),
      height: Mixins.scaleSize(50),
      borderRadius: Mixins.scaleSize(25),
      backgroundColor: Colors.WHITE,
      shadowColor: "#000",
      shadowOffset: {
	      width: 0,
	      height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,      
    },
    blackLine: {
      position: 'absolute',
      width: Spacing.SCALE_18,
      borderBottomWidth: scaleSize(2.25),
      margin: 0,
      padding: 0
    }
});

export default styles;