import * as React from 'react';
import { View, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import styles from './Styles';
import Challenge from '../../components/Challenge';
import { feedData as data } from '../../constants/ExampleData';

export type Props = {
  navigation: any;
};

// uri={'https://storage.googleapis.com/vyzov-test-bucket/video/test-video1.mp4'}
// isHaveBackground
const Feed: React.FC<Props> = ({ navigation }) => {
  const keyExtractor = ({ id }:any) => id;
  const renderItem = ({ item }: any) => (
    <Challenge data={item} isFocused={item.isFocused}/>
  )
  const addPostHandler = () => {
    console.log("Вызвали модалку")
  }

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <FlatList
          keyExtractor={keyExtractor} 
          data={data}
          renderItem={renderItem}
        />
      </ScrollView>
      <TouchableOpacity
          style={styles.addPostBtn}
          onPress={addPostHandler}
          activeOpacity={0.9}
        >
          <View style={styles.blackLine}/>
          <View style={[styles.blackLine,{transform: [{ rotate: "90deg" }]}]}/>   
      </TouchableOpacity>    
    </View>
  );
}

export default Feed