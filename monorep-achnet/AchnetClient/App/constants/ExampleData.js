export const feedData = [
    {
      id: '12',
      creator: '4342',
      isFocused: false,
      descriptionText: `название описание  челенджа хэштеги \n#snow #snowboard #mountin`,
      avatarURL: 'https://storage.googleapis.com/vyzov-test-bucket/icons/anonymous-avatar.png',
      chalengeContent: [{
        id: '121',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/skate-test-video.mp4',
        isFocused: false
      },
      {
        id: '242',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/test-video1.mp4',
        isFocused: false
      },
      {
        id: '353',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/skate-test-video.mp4',
        isFocused: false
      }]
    },
    {
      id: '13',
      creator: '4342',
      isFocused: false,
      descriptionText: `прокатиться в горах\n#snow #mountains`,
      avatarURL: 'https://storage.googleapis.com/vyzov-test-bucket/icons/avatar-example.png',
      chalengeContent: [{
        id: '121',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/skate-test-video.mp4',
        isFocused: false
      },
      {
        id: '242',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/test-video1.mp4',
        isFocused: false
      },
      {
        id: '353',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/skate-test-video.mp4',
        isFocused: false
      }]
    },
    {
      id: '14',
      creator: '4342',
      isFocused: false,
      descriptionText: `Завести котейку\n#animals`,
      avatarURL: 'https://storage.googleapis.com/vyzov-test-bucket/icons/anonymous-avatar.png',
      chalengeContent: [{
        id: '121',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/skate-test-video.mp4',
        isFocused: false
      },
      {
        id: '242',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/test-video1.mp4',
        isFocused: false
      },
      {
        id: '356',
        uri: 'https://storage.googleapis.com/vyzov-test-bucket/video/skate-test-video.mp4',
        isFocused: false
      }]
    },
]