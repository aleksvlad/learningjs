/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

export type RootStackParamList = {
  Root: undefined;
  Greeting: undefined;
  Login: undefined;
  SignUp: undefined;
  Interests: undefined;
  Feed: undefined;
  Profile: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  OpeningScreen: undefined;
  Login: undefined;
  SignUp: undefined;
  Interests: undefined;
  Feed: undefined;
  Profile: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};

export type OpeningParamList = {
  OpeningScreen: undefined;
};

export type LoginParamList = {
  LoginScreen: undefined;
};

export type SignUpParamList = {
  SignUpScreen: undefined;
};

export type InterestsParamList = {
  InterestsScreen: undefined;
}

export type FeedParamList = {
  FeedScreen: undefined;
}

export type ProfileParamList = {
  ProfileScreen: undefined;
}