/**
 * If you are not familiar with React Navigation, check out the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';
import { Text } from 'react-native';
import NotFoundScreen from '../screens/NotFound';
import { RootStackParamList } from '../constants/types';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';
import Greeting from '../screens/Greeting';
import Login from '../screens/Login';
import SignUp from '../screens/SignUp';
import Interests from '../screens/Interests';
import Feed from '../screens/Feed';
import Profile from '../screens/Profile';
import HeaderLogo from '../components/Header/HeaderLogo';
import { BottomTabParamList, FeedParamList, InterestsParamList, LoginParamList, OpeningParamList, ProfileParamList, SignUpParamList, TabOneParamList, TabTwoParamList } from '../constants/types';
import HeaderBtnPanel from '../components/Header/HeaderBtnPanel';
import { Mixins } from '../styles';

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Root" component={OpeningNavigator} />
      <Stack.Screen name="Greeting" component={Greeting} />
      <Stack.Screen name="Login" component={LoginNavigator} />
      <Stack.Screen name="SignUp" component={SignUpNavigator} />
      <Stack.Screen name="Interests" component={InterestsNavigator} />
      <Stack.Screen name="Feed" component={FeedNavigator} />
      <Stack.Screen name="Profile" component={ProfileNavigator} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
    </Stack.Navigator>
  );
}

const OpeningStack = createStackNavigator<OpeningParamList>();

function OpeningNavigator() {
  return (
    <OpeningStack.Navigator>
      <OpeningStack.Screen
        name="OpeningScreen"
        component={Greeting}
        options={{ headerShown: false }}
      />
    </OpeningStack.Navigator>
  );
}

const LoginStack = createStackNavigator<LoginParamList>();

function LoginNavigator() {
  return (
    <LoginStack.Navigator>
      <LoginStack.Screen
        name="LoginScreen"
        component={Login}
        options={{ headerShown: false }}
      />
    </LoginStack.Navigator>
  );
}

const SignUpStack = createStackNavigator<SignUpParamList>();

function SignUpNavigator() {
  return (
    <SignUpStack.Navigator>
      <SignUpStack.Screen
        name="SignUpScreen"
        component={SignUp}
        options={{ headerShown: false }}
      />
    </SignUpStack.Navigator>
  );
}

const InterestsStack = createStackNavigator<InterestsParamList>();

function InterestsNavigator() {
  return (
    <InterestsStack.Navigator>
      <InterestsStack.Screen
        name="InterestsScreen"
        component={Interests}
        options={{ headerShown: false }}
      />
    </InterestsStack.Navigator>
  );
}

const FeedStack = createStackNavigator<FeedParamList>();

function FeedNavigator() {
  return (
    <FeedStack.Navigator>
      <FeedStack.Screen
        name="FeedScreen"
        component={Feed}
        options={{
          headerLeft: props => <HeaderLogo />,
          headerRight: props => <HeaderBtnPanel />,
          title: '',
          headerStyle: {
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 10,
            },
            shadowOpacity: 0.51,
            shadowRadius: 13.16,
            elevation: 10,
          }
        }}
      />
    </FeedStack.Navigator>
  );
}

const ProfileStack = createStackNavigator<ProfileParamList>();

function ProfileNavigator() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="ProfileScreen"
        component={Profile}
        options={{
          headerLeft: props => <HeaderLogo />,
          headerRight: props => <HeaderBtnPanel />,
          title: '',
          headerStyle: {
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 10,
            },
            shadowOpacity: 0.51,
            shadowRadius: 13.16,
            elevation: 10,
          }
        }}
      />
    </ProfileStack.Navigator>
  );
}