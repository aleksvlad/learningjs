/**
 * Learn more about createBottomTabNavigator:
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */

import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import Greeting from '../screens/Greeting';
import Login from '../screens/Login';
import SignUp from '../screens/SignUp';
import Interests from '../screens/Interests';
import Feed from '../screens/Feed';
import Profile from '../screens/Profile';
import { BottomTabParamList, FeedParamList, InterestsParamList, LoginParamList, OpeningParamList, ProfileParamList, SignUpParamList, TabOneParamList, TabTwoParamList } from '../constants/types';
import { Text } from 'react-native';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="OpeningScreen"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
      <BottomTab.Screen
        name="OpeningScreen"
        component={OpeningTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Login"
        component={LoginTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="SignUp"
        component={SignUpTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Interests"
        component={InterestsTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Feed"
        component={FeedNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const OpeningStack = createStackNavigator<OpeningParamList>();

function OpeningTabNavigator() {
  return (
    <OpeningStack.Navigator>
      <OpeningStack.Screen
        name="OpeningScreen"
        component={Greeting}
        options={{ headerTitle: 'Opening' }}
      />
    </OpeningStack.Navigator>
  );
}

const LoginStack = createStackNavigator<LoginParamList>();

function LoginTabNavigator() {
  return (
    <LoginStack.Navigator>
      <LoginStack.Screen
        name="LoginScreen"
        component={Login}
        options={{ headerTitle: 'Login' }}
      />
    </LoginStack.Navigator>
  );
}

const SignUpStack = createStackNavigator<SignUpParamList>();

function SignUpTabNavigator() {
  return (
    <SignUpStack.Navigator>
      <SignUpStack.Screen
        name="SignUpScreen"
        component={SignUp}
        options={{ headerTitle: 'SignUp' }}
      />
    </SignUpStack.Navigator>
  );
}

const InterestsStack = createStackNavigator<InterestsParamList>();

function InterestsTabNavigator() {
  return (
    <InterestsStack.Navigator>
      <InterestsStack.Screen
        name="InterestsScreen"
        component={Interests}
        options={{ headerTitle: 'Interests Title' }}
      />
    </InterestsStack.Navigator>
  );
}

const FeedStack = createStackNavigator<FeedParamList>();

function FeedNavigator() {
  return (
    <FeedStack.Navigator>
      <FeedStack.Screen
        name="FeedScreen"
        component={Feed}
        // options={{
        //   headerTitle: props => <LogoTitle {...props} />,
        //   headerRight: () => (
        //     <Button
        //       onPress={() => alert('This is a button!')}
        //       title="Info"
        //       color="#fff"
        //     />
        //   ),
        // }}
      />
    </FeedStack.Navigator>
  );
}

const ProfileStack = createStackNavigator<ProfileParamList>();

function ProfileNavigator() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="ProfileScreen"
        component={Profile}
        options={{
          headerTitle: props => <Text>Profile</Text>,
        //   headerRight: () => (
        //     <Button
        //       onPress={() => alert('This is a button!')}
        //       title="Info"
        //       color="#fff"
        //     />
        //   ),
        }}
      />
    </ProfileStack.Navigator>
  );
}