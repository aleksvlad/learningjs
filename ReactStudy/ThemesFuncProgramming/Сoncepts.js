let log = function (message) {
    console.log(message)
};
log("In JavaScript functions are variables")
// Функции в JavaScript являются переменными

const log = message => console.log(message)

const obj = {
    message: "They can be added to objects like variables",
    log(message) {
        console.log(message)
    }
}

obj.log(obj.message)

const messages = [
    "They can be inserted into arrays",
    message => console.log(message),
    "like variables",
    message => console.log(message)
]
messages[1](messages[0]) // Они могут вставляться в массивы
messages[3](messages[2]) // как переменные

const insideFn = logger =>
    logger("They can be sent to other functions as arguments");
insideFn(message => console.log(message))
// Они могут отправляться другим функциям в качестве аргументов
// Они точно так же, как и переменные, могут возвращаться из других функций:

let createScream = function (logger) {
    return function (message) {
        logger(message.toUpperCase() + "!!!")
    }
}

const scream = createScream(message => console.log(message))
scream('functions can be returned from other functions')
scream('createScream returns a function')
scream('scream invokes that returned function')
// ФУНКЦИИ МОГУТ ВОЗВРАЩАТЬСЯ ИЗ ДРУГИХ ФУНКЦИЙ!!!
// CREATESCREAM ВОЗВРАЩАЕТ ФУНКЦИЮ!!!
// SCREAM ВЫЗЫВАЕТ ВОЗВРАЩЕННУЮ ФУНКЦИЮ!!!

// Последние два примера были функциями высшего порядка, то есть функциями,
// которые могут либо получать, либо возвращать другие функции. Используя
// синтаксис ES6, можно дать описание точно такой же функции высшего порядка
// createScream с помощью стрелок:
const createScream = logger => message =>
    logger(message.toUpperCase() + "!!!")