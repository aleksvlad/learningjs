var frederick = {
    name: "Frederick Douglass",
    canRead: false,
    canWrite: false
}

function selfEducate() {
    frederick.canRead = true
    frederick.canWrite = true
    return frederick
}

selfEducate()
console.log(frederick)
// {name: "Frederick Douglass", canRead: true, canWrite: true}

const frederick = {
    name: "Frederick Douglass",
    canRead: false,
    canWrite: false
}

const selfEducate = (person) => {
    person.canRead = true
    person.canWrite = true
    return person
}

console.log(selfEducate(frederick))
console.log(frederick)

// {name: "Frederick Douglass", canRead: true, canWrite: true}
// {name: "Frederick Douglass", canRead: true, canWrite: true}

const frederick = {
    name: "Frederick Douglass",
    canRead: false,
    canWrite: false
}

const selfEducate = person =>
    ({
        ...person,
        canRead: true,
        canWrite: true
    })

console.log(selfEducate(frederick))
console.log(frederick)
// {name: "Frederick Douglass", canRead: true, canWrite: true}
// {name: "Frederick Douglass", canRead: false, canWrite: false}

function Header(text) {
    let h1 = document.createElement('h1');
    h1.innerText = text;
    document.body.appendChild(h1);
}

Header("Header() caused side effects");

const Header = (props) => <h1>{props.title}</h1>