const schools = [
    "Yorktown",
    "Washington & Lee",
    "Wakefield"
]

console.log(schools.join(", "))
// "Yorktown, Washington & Lee, Wakefield"

const wSchools = schools.filter(school => school[0] === "W")
console.log(wSchools)
// ["Washington & Lee", "Wakefield"]

// Delete
const cutSchool = (cut, list) =>
    list.filter(school => school !== cut)
console.log(cutSchool("Washington & Lee", schools).join(" * "))
// "Yorktown * Wakefield"
console.log(schools.join("\n"))
// Yorktown
// Washington & Lee
// Wakefield

const highSchools = schools.map(school => `${school} High School`)
console.log(highSchools.join("\n"))
// Yorktown High School
// Washington & Lee High School
// Wakefield High School
console.log(schools.join("\n"))
// Yorktown
// Washington & Lee
// Wakefield

const highSchools = schools.map(school => ({ name: school }))
console.log(highSchools)

// [
// { name: "Yorktown" },
// { name: "Washington & Lee" },
// { name: "Wakefield" }
// ]

let schools = [
    { name: "Yorktown" },
    { name: "Stratford" },
    { name: "Washington & Lee" },
    { name: "Wakefield" }
]

let updatedSchools = editName("Stratford", "HB Woodlawn", schools)
console.log(updatedSchools[1]) // { name: "HB Woodlawn" }
console.log(schools[1]) // { name: "Stratford" },

const editName = (oldName, name, arr) =>
    arr.map(item => {
        if (item.name === oldName) {
            return {
                ...item,
                name
            }
        } else {
            return item
        }
    })

// edit
const editName = (oldName, name, arr) =>
    arr.map(item => (item.name === oldName) ?
        ({ ...item, name }) :
        item
    )

const schools = {
    "Yorktown": 10,
    "Washington & Lee": 2,
    "Wakefield": 5
}
const schoolArray = Object.keys(schools).map(key =>
    ({
        name: key,
        wins: schools[key]
    })
)

console.log(schoolArray)
// [
// {
// name: "Yorktown",
// wins: 10
// },
// {
// name: "Washington & Lee",
// wins: 2
// },
// {
// name: "Wakefield",
// wins: 5
// }
// ]

const ages = [21, 18, 42, 40, 64, 63, 34];
const maxAge = ages.reduce((max, age) => {
    console.log(`${age} > ${max} = ${age > max}`);
    if (age > max) {
        return age
    } else {
        return max
    }
}, 0)

console.log('maxAge', maxAge);
// 21 > 0 = true
// 18 > 21 = false
// 42 > 21 = true
// 40 > 42 = false
// 64 > 42 = true
// 63 > 64 = false
// 34 > 64 = false
// maxAge 64

const max = ages.reduce(
    (max, value) => (value > max) ? value : max,
    0
)

const colors = [
    {
        id: '-xekare',
        title: "rad red",
        rating: 3
    },
    {
        id: '-jbwsof',
        title: "big blue",
        rating: 2
    },
    {
        id: '-prigbj',
        title: "grizzly grey",
        rating: 5
    },
    {
        id: '-ryhbhsl',
        title: "banana",
        rating: 1
    }
]

const hashColors = colors.reduce(
    (hash, { id, title, rating }) => {
        hash[id] = { title, rating }
        return hash
    },
    {}
)
console.log(hashColors);
// {
// "-xekare": {
// title:"rad red",
// rating:3
// },
// "-jbwsof": {
// title:"big blue",
// rating:2
// },
// "-prigbj": {
// title:"grizzly grey",
// rating:5
// },
// "-ryhbhsl": {
// title:"banana",
// rating:1
// }
// }

const colors = ["red", "red", "green", "blue", "green"];
const distinctColors = colors.reduce(
    (distinct, color) =>
        (distinct.indexOf(color) !== -1) ?
            distinct :
            [...distinct, color],
    []
)

console.log(distinctColors)
// ["red", "green", "blue"]