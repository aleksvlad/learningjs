const invokeIf = (condition, fnTrue, fnFalse) =>
    (condition) ? fnTrue() : fnFalse()
const showWelcome = () =>
    console.log("Welcome!!!")
const showUnauthorized = () =>
    console.log("Unauthorized!!!")
invokeIf(true, showWelcome, showUnauthorized) // "Welcome"
invokeIf(false, showWelcome, showUnauthorized) // "Unauthorized"

const userLogs = userName => message =>
    console.log(`${userName} -> ${message}`)
const log = userLogs("grandpa23")
log("attempted to load 20 fake members")

getFakeMembers(20).then(
    members => log(`successfully loaded ${members.length} members`),
    error => log("encountered an error loading members")
)
// grandpa23 -> попытка загрузки 20 мнимых сотрудников
// grandpa23 -> успешно загружены 20 сотрудников
// grandpa23 -> попытка загрузки 20 мнимых сотрудников
// grandpa23 -> при загрузке сотрудников возникла ошибка

