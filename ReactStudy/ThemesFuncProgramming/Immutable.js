let color_lawn = {
    title: "lawn",
    color: "#00FF00",
    rating: 0
}

//Можно создать функцию, оценивающую цвета, и использовать ее для изменения рейтинга объекта color:
function rateColor(color, rating) {
    color.rating = rating
    return color
}

console.log(rateColor(color_lawn, 5).rating) // 5
console.log(color_lawn.rating) // 5

// Функцию rateColor можно переписать, чтобы она не наносила вред оригиналу (объекту color):

let rateColor = function (color, rating) {
    return Object.assign({}, color, { rating: rating })
}

console.log(rateColor(color_lawn, 5).rating) // 5
console.log(color_lawn.rating) // 4

//Точно такую же функцию можно создать с помощью стрелочной функции ES6 и оператора распространения объекта ES7
const rateColor = (color, rating) =>
    ({
        ...color,
        rating
    })

// Рассмотрим массив названий цветов:
let list = [
    { title: "Rad Red" },
    { title: "Lawn" },
    { title: "Party Pink" }
]

// Можно создать функцию, которая будет добавлять цвета к массиву, используя Array.push:

let addColor = function (title, colors) {
    colors.push({ title: title })
    return colors;
}

console.log(addColor("Glam Green", list).length) // 4
console.log(list.length) // 4

// add
const addColor = (title, array) => array.concat({ title })
console.log(addColor("Glam Green", list).length) // 4
console.log(list.length) // 3

// add
const addColor = (title, list) => [...list, { title }]
// Но Array.push не является неизменяемой функцией.