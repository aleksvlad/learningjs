const countdown = (value, fn) => {
    fn(value)
    return (value > 0) ? countdown(value - 1, fn) : value
}
countdown(10, value => console.log(value));
// 10
// 9
// 8
// 7
// 6
// 5
// 4'

const countdown = (value, fn, delay = 1000) => {
    fn(value)
    return (value > 0) ?
        setTimeout(() => countdown(value - 1, fn), delay) :
        value
}

const log = value => console.log(value)
countdown(10, log);

let dan = {
    type: "person",
    data: {
        gender: "male",
        info: {
            id: 22,
            fullname: {
                first: "Dan",
                last: "Deacon"
            }
        }
    }
}

deepPick("type", dan); // "person"
deepPick("data.info.fullname.first", dan); // "Dan"

const deepPick = (fields, object = {}) => {
    const [first, ...remaining] = fields.split(".")
    return (remaining.length) ?
        deepPick(remaining.join("."), object[first]) :
        object[first]
}

//В следующем фрагменте кода можно увидеть, 
//как по мере итераций, выполняемых функцией deepPick, изменяются значения для first, remaining и object[first]:
deepPick("data.info.fullname.first", dan); // "Deacon"
// Первая итерация
// first = "data"
// remaining.join(".") = "info.fullname.first"
// object[first] = { gender: "male", {info} }
// Вторая итерация
// first = "info"
// remaining.join(".") = "fullname.first"
// object[first] = {id: 22, {fullname}}
// Третья итерация
// first = "fullname"
// remaining.join("." = "first"
// object[first] = {first: "Dan", last: "Deacon" }
// Наконец...
// first = "first"
// remaining.length = 0
// object[first] = "Deacon"

