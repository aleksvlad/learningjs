const fs = require("fs")

let CesarRu = new
    (function CesarRU(offset) {
        let ACode = "а".charCodeAt(0), alphabetSize = 31, Self = this;
        this.encodeChar = (char) => { return (char + offset - ACode + alphabetSize) % alphabetSize + ACode }
        this.decodeChar = (char) => { return (char - offset - ACode + alphabetSize) % alphabetSize + ACode }
        this.encode = (str) => {
            let res = "", char
            for (var a = 0; a < str.length; ++a)
                if ((char = str.charCodeAt(a)) && (char >= ACode) && (char - ACode < alphabetSize))
                    res += String.fromCharCode(Self.encodeChar(char))
                else res += str.charAt(a)
            return res
        }
        this.decode = (str) => {
            var res = ""
            for (var a = 0; a < str.length; ++a)
                if ((chr = str.charCodeAt(a)) && (chr >= ACode) && (chr - ACode < alphabetSize))
                    res += String.fromCharCode(Self.decodeChar(chr))
                else res += str.charAt(a)
            return res
        }
    })(3)

console.log(CesarRu.encode("привет"))
console.log(CesarRu.decode(CesarRu.encode("привет")))

let CesarEn = new
    (function CesarEN(offset) {
        let ACode = "a".charCodeAt(0), alphabetSize = 26, Self = this;
        this.encodeChar = (char) => { return (char + offset - ACode + alphabetSize) % alphabetSize + ACode }
        this.decodeChar = (char) => { return (char - offset - ACode + alphabetSize) % alphabetSize + ACode }
        this.encode = (str) => {
            let res = "", chr
            for (var a = 0; a < str.length; ++a)
                if ((chr = str.charCodeAt(a)) && (chr >= ACode) && (chr - ACode < alphabetSize))
                    res += String.fromCharCode(Self.encodeChar(chr))
                else res += str.charAt(a)
            return res
        }
        this.decode = (str) => {
            var res = ""
            for (var a = 0; a < str.length; ++a)
                if ((chr = str.charCodeAt(a)) && (chr >= ACode) && (chr - ACode < alphabetSize))
                    res += String.fromCharCode(Self.decodeChar(chr))
                else res += str.charAt(a)
            return res
        }
    })(3)

console.log(CesarEn.encode("hello! How are you?"))
console.log(CesarEn.decode("khoor! Hrz duh brx?"))

const codeTextInFile = (lang) => {
    let fileContent = fs.readFileSync("code.txt", "utf8",
        function (error, data) {
            console.log("Асинхронное чтение файла")
            if (error) throw error // если возникла ошибка
            console.log(data)  // выводим считанные данные
    })

    if(lang === 'ru'){
        const enc = CesarRu.encode(fileContent)
        fs.writeFile("encode.txt", enc, function (error) {
            if (error) throw error; // если возникла ошибка
            console.log("Асинхронная запись файла завершена. Содержимое файла:")
            let data = fs.readFileSync("encode.txt", "utf8")
            console.log(data)  // выводим считанные данные
        })
    } else if (lang === 'en') {
        const enc = CesarEn.encode(fileContent)

        fs.writeFile("encode.txt", enc, function (error) {
            if (error) throw error; // если возникла ошибка
            console.log("Асинхронная запись файла завершена. Содержимое файла:")
            let data = fs.readFileSync("encode.txt", "utf8")
            console.log(data)  // выводим считанные данные
        })
    }
}

const encodeTextInFile = (lang) => {
    let fileContent = fs.readFileSync("encode.txt", "utf8",
        function (error, data) {
            console.log("Асинхронное чтение файла")
            if (error) throw error // если возникла ошибка
            console.log(data)  // выводим считанные данные
    })

    if(lang === 'ru'){
        const enc = CesarRu.decode(fileContent)

        fs.writeFileSync("encode.txt", enc, function (error) {
            if (error) throw error; // если возникла ошибка
            console.log("Асинхронная запись файла завершена. Содержимое файла:")
            let data = fs.readFileSync("encode.txt", "utf8")
            console.log(data)  // выводим считанные данные
        })
    } else if (lang === 'en') {
        const enc = CesarEn.decode(fileContent)

        fs.writeFileSync("encode.txt", enc, function (error) {
            if (error) throw error; // если возникла ошибка
            console.log("Асинхронная запись файла завершена. Содержимое файла:")
            let data = fs.readFileSync("encode.txt", "utf8")
            console.log(data)  // выводим считанные данные
        })
    }
}

// Шифровка расшифровка файла

// codeTextInFile('ru')
// encodeTextInFile('ru')

// codeTextInFile('en')
encodeTextInFile('en')


// // асинхронное чтение
// fs.readFile("text.txt", "utf8",
//     function (error, data) {
//         console.log("Асинхронное чтение файла");
//         if (error) throw error; // если возникла ошибка
//         console.log(data);  // выводим считанные данные
//     });

// // синхронное чтение
// console.log("Синхронное чтение файла")
// let fileContent = fs.readFileSync("text.txt", "utf8");
// console.log(fileContent);

// fs.writeFile("encode.txt", "Hello мир!", function (error) {

//     if (error) throw error; // если возникла ошибка
//     console.log("Асинхронная запись файла завершена. Содержимое файла:");
//     let data = fs.readFileSync("encode.txt", "utf8");
//     console.log(data);  // выводим считанные данные
// });