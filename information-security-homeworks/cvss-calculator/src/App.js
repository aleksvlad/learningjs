import React from 'react'
import './App.css'
import CalculatorCVSSv2 from './components/CalculatorCVSSv2'
import CalculatorCVSSv3 from './components/CalculatorCVSSv3'

const TabContent = ({ title, content }) => (
  <div className="tabcontent">
    <h3>{title}</h3>
    <p>{content}</p>
  </div>
);

const Tabs = ({ tabs }) => {
  const [active, setActive] = React.useState('СVSSv2');

  const openTab = e => setActive(+e.target.dataset.index);

  console.log(tabs)

  return (
    <div>
      <div className="tab">
        {tabs.map((i) => (
          <button
            type="button"
            class="btn btn-secondary"
            className={`tablinks ${i === active ? 'active' : ''}`}
            onClick={openTab}
            data-index={i}
          >{i.title}</button>
        ))}
      </div>
      {/* {tabs[active] && <TabContent {...tabs[active]} />} */}
    </div>
  );
}

const tabs = [
  { title: 'СVSSv2' },
  { title: 'СVSSv3' }
];

const App = () => {
  return (
    <div id='App'>
      <Tabs tabs={tabs} />
      <CalculatorCVSSv2 />
      {/* <CalculatorCVSSv3 /> */}
    </div>
  )
}

export default App;
