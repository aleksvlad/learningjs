import React, { Component, useState } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';

const Chart = ({ chartData }) => {

  const [legendPosition, setLegendPosition] = useState('right')
  const [displayTitle, setDisplayTitle] = useState(true)
  const [displayLegend, setDisplayLegend] = useState(true)

  console.log(chartData)

  return (
    <div className="chart">
      <Bar
        data={chartData}
        options={{
          title: {
            display: displayTitle,
            text: 'La',
            fontSize: 25
          },
          legend: {
            display: displayLegend,
            position: legendPosition
          }
        }}
      />

      <Line
        data={chartData}
        options={{
          title: {
            display: displayTitle,
            text: 'La',
            fontSize: 5
          },
          legend: {
            display: displayLegend,
            position: legendPosition
          }
        }}
      />

      <Pie
        data={chartData}
        options={{
          title: {
            display: displayTitle,
            text: 'La',
            fontSize: 25
          },
          legend: {
            display: displayLegend,
            position: legendPosition
          }
        }}
      />
    </div>
  )
}

export default Chart