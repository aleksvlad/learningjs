import React, { useState, useEffect } from 'react'
import { impact, exploitability, fImpact, baseScore, temporalScore, adjustedBaseScore, adjustedImpact, adjustedTemporal, environmentalScore, showHazardLevelFortiGuard } from '../utils/CVSSv2'

const CalculatorCVSSv3 = () => {

    // Базовые метрики
    const [AV, setAV] = useState(null)
    const [AC, setAC] = useState(null)
    const [Au, setAu] = useState(null)
    const [C, setC] = useState(null)
    const [I, setI] = useState(null)
    const [A, setA] = useState(null)

    // Временные метрики
    const [E, setE] = useState(null)
    const [RL, setRL] = useState(null)
    const [RC, setRC] = useState(null)

    // Контекстные метрики
    const [CDP, setCDP] = useState(null)
    const [TD, setTD] = useState(null)
    const [CR, setCR] = useState(null)
    const [IR, setIR] = useState(null)
    const [AR, setAR] = useState(null)

    useEffect(() => {

    }, [])

    {/* <button id={id} className='del-button'
            onClick={
                useDeleteClock(id)
            }>
        </button>
        
        <tr className={'MAV'} id={'MAV'} onClick={()=>}>
 
        <tr className={'MAV'} id={'MAV'} > либо так 
            и еще вот это надо document.getElementById('MAC').onclick = (event) => {
            this.setState({MAC: event.target.value});
        };
        
         */}

    const impactValue = impact(C, I, A)
    const exploitabilityValue = exploitability(AV, AC, Au)
    const fImpactValue = fImpact(impactValue)
    const baseScoreValue = baseScore(impactValue, exploitabilityValue, fImpactValue)

    const temporalScoreValue = temporalScore(baseScoreValue, E, RL, RC)

    const adjustedImpactValue = adjustedImpact(C, CR, I, IR, A, AR)
    const adjustedBaseScoreValue = adjustedBaseScore(adjustedImpactValue, exploitability, fImpactValue)
    const adjustedTemporalValue = adjustedTemporal(adjustedBaseScoreValue, E, RL, RC)

    const environmentalScoreValue = environmentalScore(adjustedTemporalValue, CDP, TD)

    // console.log()

    return (

        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" title="Базовые метрики">
                            Базовые метрики <span class="glyphicon glyphicon-question-sign help-hint" data-toggle="popover" title="Базовые метрики" data-content="метрики, отражающие основные характеристики уязвимости, влияющие на доступность, целостность и конфиденциальность информации, которые не изменяются во времени и не зависят от среды функционирования программного обеспечения."></span>
                            <span class="label label-success bs-result-val"></span>
                            <small class="cvss_bs pull-right"></small>
                        </a>
                    </h4>
                </div>

                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div id="bs-warning" class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Внимание!</strong> Для получения результата необходимо выбрать значение каждого критерия!
            </div>
                        <div id="bs-result" class="alert alert-success">
                            <strong>Базовая оценка (BS):</strong> <strong class="bs-result-val">${baseScoreValue}</strong>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="col-lg-12">
                                    <h4>Способ получения доступа (AV):</h4>
                                    <div class="btn-group" role="group" aria-label="Basic example" >
                                        <button type="button" class="btn btn-outline-success" data-BS-Group="AV" data-val="L" onClick={(e) => { setAV(0.395) }}>Локальный (L)</button>
                                        <button type="button" class="btn btn-outline-warning" data-BS-Group="AV" data-val="A" onClick={(e) => { setAV(0.646) }}>Смежная сеть (A)</button>
                                        <button type="button" class="btn btn-outline-danger" data-BS-Group="AV" data-val="N" onClick={(e) => { setAV(1.0) }}>Сетевой (N)</button>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <h4>Сложность получения доступа (AC):</h4>
                                    <div class="btn-group btn-group-justified">
                                        <button type="button" class="btn btn-outline-success" data-BS-Group="AC" data-val="H" onClick={(e) => { setAC(0.35) }}>Высокая (H)</button>
                                        <button type="button" class="btn btn-outline-warning" data-BS-Group="AC" data-val="M" onClick={(e) => { setAC(0.61) }}>Средняя (M)</button>
                                        <button type="button" class="btn btn-outline-danger" data-BS-Group="AC" data-val="L" onClick={(e) => { setAC(0.71) }}>Низкая (L)</button>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <h4>Аутентификация (Au):</h4>
                                    <div class="btn-group btn-group-justified">
                                        <button type="button" class="btn btn-outline-success" data-BS-Group="Au" data-val="M" onClick={(e) => { setAu(0.45) }}>Множественная (M)</button>
                                        <button type="button" class="btn btn-outline-warning" data-BS-Group="Au" data-val="S" onClick={(e) => { setAu(0.56) }}>Единственная (S)</button>
                                        <button type="button" class="btn btn-outline-danger" data-BS-Group="Au" data-val="N" onClick={(e) => { setAu(0.704) }}>Не требуется (N)</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="col-lg-12">
                                    <h4>Влияние на конфиденциальность (С):</h4>
                                    <div class="btn-group btn-group-justified">
                                        <button type="button" class="btn btn-outline-success" data-BS-Group="C" data-val="N" onClick={(e) => { setC(0.0) }}>Не оказывает (N)</button>
                                        <button type="button" class="btn btn-outline-warning" data-BS-Group="C" data-val="P" onClick={(e) => { setC(0.275) }}>Частичное (P)</button>
                                        <button type="button" class="btn btn-outline-danger" data-BS-Group="C" data-val="C" onClick={(e) => { setC(0.66) }}>Полное (C)</button>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <h4>Влияние на целостность (I):</h4>
                                    <div class="btn-group btn-group-justified">
                                        <button type="button" class="btn btn-outline-success" data-BS-Group="I" data-val="N" onClick={(e) => { setI(0.0) }}>Не оказывает (N)</button>
                                        <button type="button" class="btn btn-outline-warning" data-BS-Group="I" data-val="P" onClick={(e) => { setI(0.275) }}>Частичное (P)</button>
                                        <button type="button" class="btn btn-outline-danger" data-BS-Group="I" data-val="C" onClick={(e) => { setI(0.66) }}>Полное (C)</button>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <h4>Влияние на доступность (A):</h4>
                                    <div class="btn-group btn-group-justified">
                                        <button type="button" class="btn btn-outline-success" data-BS-Group="A" data-val="N" onClick={(e) => { setA(0.0) }}>Не оказывает (N)</button>
                                        <button type="button" class="btn btn-outline-warning" data-BS-Group="A" data-val="P" onClick={(e) => { setA(0.275) }}>Частичное (P)</button>
                                        <button type="button" class="btn btn-outline-danger" data-BS-Group="A" data-val="C" onClick={(e) => { setA(0.66) }}>Полное (C)</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" title="Временные метрики">
                            Временные метрики                    <span class="glyphicon glyphicon-question-sign help-hint" data-toggle="popover" title="Временные метрики" data-content="метрики, отражающие характеристики уязвимости, которые изменяются со временем, но не зависят от среды функционирования программного обеспечения."></span>
                            <span class="label label-success ts-result-val"></span>
                            <small class="cvss_ts pull-right"></small>
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div id="ts-warning" class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Внимание!</strong> Для получения результата необходимо выбрать значение каждого критерия<span id="ts-warning-bs">, а также рассчитать базовую метрику</span>!
            </div>
                        <div id="ts-result" class="alert alert-success">
                            <strong>Временная оценка (TS):</strong> <strong class="ts-result-val">${temporalScoreValue}</strong>
                        </div>

                        <div class="col-lg-12">
                            <h4>Возможость использования (E):</h4>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-outline-danger" data-TS-Group="E" data-val="ND" onClick={(e) => { setE(1.0) }}>Не определено (ND)</button>
                                <button type="button" class="btn btn-outline-success" data-TS-Group="E" data-val="U" onClick={(e) => { setE(0.85) }}>Теоретически (U)</button>
                                <button type="button" class="btn btn-outline-warning" data-TS-Group="E" data-val="POC" onClick={(e) => { setE(0.9) }}>Есть концепция (POC)</button>
                                <button type="button" class="btn btn-outline-warning" data-TS-Group="E" data-val="F" onClick={(e) => { setE(0.95) }}>Есть сценарий (F)</button>
                                <button type="button" class="btn btn-outline-danger" data-TS-Group="E" data-val="H" onClick={(e) => { setE(1.0) }}>Высокая (H)</button>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h4>Уровень исправления (RL):</h4>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-outline-danger" data-TS-Group="RL" data-val="ND" onClick={(e) => { setRL(1.0) }}>Не определено (ND)</button>
                                <button type="button" class="btn btn-outline-success" data-TS-Group="RL" data-val="OF" onClick={(e) => { setRL(0.87) }}>Официальное (OF)</button>
                                <button type="button" class="btn btn-outline-success" data-TS-Group="RL" data-val="T" onClick={(e) => { setRL(0.9) }}>Временное (T)</button>
                                <button type="button" class="btn btn-outline-warning" data-TS-Group="RL" data-val="W" onClick={(e) => { setRL(0.95) }}>Рекомендации (W)</button>
                                <button type="button" class="btn btn-outline-danger" data-TS-Group="RL" data-val="U" onClick={(e) => { setRL(1.0) }}>Недоступно (U)</button>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h4>Степень достоверности источника (RC):</h4>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-outline-danger" data-TS-Group="RC" data-val="ND" onClick={(e) => { setRC(1.0) }}>Не определено (ND)</button>
                                <button type="button" class="btn btn-outline-success" data-TS-Group="RC" data-val="UC" onClick={(e) => { setRC(0.9) }}>Не подтверждена (UC)</button>
                                <button type="button" class="btn btn-outline-warning" data-TS-Group="RC" data-val="UR" onClick={(e) => { setRC(0.95) }}>Не доказана (UR)</button>
                                <button type="button" class="btn btn-outline-danger" data-TS-Group="RC" data-val="C" onClick={(e) => { setRC(1.0) }}>Подтверждена (C)</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" title="Контекстные метрики">
                            Контекстные метрики                    <span class="glyphicon glyphicon-question-sign help-hint" data-toggle="popover" title="Контекстные метрики" data-content="метрики, отражающие  характеристики уязвимости, зависящие от среды функционирования программного обеспечения."></span>
                            <span class="label label-success es-result-val"></span>
                            <small class="cvss_es pull-right"></small>
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div id="es-warning" class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Внимание!</strong> Для получения результата необходимо выбрать значение каждого критерия<span id="es-warning-ts">, а также выбрать критерии временной метрики</span><span id="es-warning-bs"> и рассчитать базовую метрику</span>!
            </div>
                        <div id="es-result" class="alert alert-success">
                            <strong>Контекстная оценка (ES):</strong> <strong class="es-result-val">${environmentalScore}</strong>
                        </div>

                        <div class="col-lg-12">
                            <h4>Вероятность нанесения косвенного ущерба (CDP):</h4>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-outline-success" data-ES-Group="CDP" data-val="ND" onClick={(e) => { setCDP(0.0) }}>Не определено (ND)</button>
                                <button type="button" class="btn btn-outline-success" data-ES-Group="CDP" data-val="N" onClick={(e) => { setCDP(0.0) }}>Отсутствует (N)</button>
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="CDP" data-val="L" onClick={(e) => { setCDP(0.1) }}>Низкая (L)</button>
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="CDP" data-val="LM" onClick={(e) => { setCDP(0.3) }}>Средняя (LM)</button>
                                <button type="button" class="btn btn-outline-danger" data-ES-Group="CDP" data-val="MH" onClick={(e) => { setCDP(0.4) }}>Повышенная (MH)</button>
                                <button type="button" class="btn btn-outline-danger" data-ES-Group="CDP" data-val="H" onClick={(e) => { setCDP(0.5) }}>Высокая (H)</button>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <h4>Плотность целей (TD):</h4>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-outline-danger" data-ES-Group="TD" data-val="ND" onClick={(e) => { setTD(1.0) }}>Не определено (ND)</button>
                                <button type="button" class="btn btn-outline-success" data-ES-Group="TD" data-val="N" onClick={(e) => { setTD(0.0) }}>Отсутствует (N)</button>
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="TD" data-val="L" onClick={(e) => { setTD(0.25) }}>Низкая (L)</button>
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="TD" data-val="M" onClick={(e) => { setTD(0.75) }}>Средняя (M)</button>
                                <button type="button" class="btn btn-outline-danger" data-ES-Group="TD" data-val="H" onClick={(e) => { setTD(1.0) }}>Высокая (H)</button>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <h4>Требования к конфиденциальности:</h4>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="CR" data-val="ND" onClick={(e) => setCR(1.0)}>Не определено (ND)</button>
                                <button type="button" class="btn btn-outline-success" data-ES-Group="CR" data-val="L" onClick={(e) => setCR(0.5)}>Низкая (L)</button>
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="CR" data-val="M" onClick={(e) => setCR(1.0)}>Средняя (M)</button>
                                <button type="button" class="btn btn-outline-danger" data-ES-Group="CR" data-val="H" onClick={(e) => setCR(1.51)}>Высокая (H)</button>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <h4>Требования к целостности:</h4>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="IR" data-val="ND" onClick={(e) => setIR(1.0)}>Не определено (ND)</button>
                                <button type="button" class="btn btn-outline-success" data-ES-Group="IR" data-val="L" onClick={(e) => setIR(0.5)}>Низкая (L)</button>
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="IR" data-val="M" onClick={(e) => setIR(1.0)}>Средняя (M)</button>
                                <button type="button" class="btn btn-outline-danger" data-ES-Group="IR" data-val="H" onClick={(e) => setIR(1.51)}>Высокая (H)</button>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <h4>Требования к доступности:</h4>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="AR" data-val="ND" onClick={(e) => setAR(1.0)}>Не определено (ND)</button>
                                <button type="button" class="btn btn-outline-success" data-ES-Group="AR" data-val="L" onClick={(e) => setAR(0.5)}>Низкая (L)</button>
                                <button type="button" class="btn btn-outline-warning" data-ES-Group="AR" data-val="M" onClick={(e) => setAR(1.0)}>Средняя (M)</button>
                                <button type="button" class="btn btn-outline-danger" data-ES-Group="AR" data-val="H" onClick={(e) => setAR(1.51)}>Высокая (H)</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default CalculatorCVSSv3