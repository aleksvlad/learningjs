(function () {
    /**
     * Корректировка округления десятичных дробей.
     *
     * @param {String}  type  Тип корректировки.
     * @param {Number}  value Число.
     * @param {Integer} exp   Показатель степени (десятичный логарифм основания корректировки).
     * @returns {Number} Скорректированное значение.
     */
    function decimalAdjust(type, value, exp) {
        // Если степень не определена, либо равна нулю...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Если значение не является числом, либо степень не является целым числом...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Сдвиг разрядов
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Обратный сдвиг
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    // Десятичное округление к ближайшему
    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    // Десятичное округление вниз
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    // Десятичное округление вверх
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})()

const roundUp = (number) => Math.ceil10(number, -1)

// Таблица 13. Числовые значения базовых метрик для CVSS V3
// Вектор атаки (AV)
// N 0,85
// A 0,62
// L 0,55
// P 0,2
// Сложность атаки (AC)
// L 0,77
// H 0,44
// Уровень привилегий (PR)
// N 0,85
// L 0,62 (0,68, если S=1)
// H 0,27 (0,50, если S=1)
// Взаимодействие с пользователем (UI):
// N 0,85
// R 0,62
// Влияние на другие компоненты системы (S):
// U 0
// C 1
// Влияние на конфиденциальность (С), Влияние на целостность (I), Влияние на
// доступность (A)
// N 0,0
// L 0,22
// H 0,56 

const exploitability = (AV, AC, PR, UI) => 8.22 * AV * AC * PR * UI

const impactBase = (C, I, A) => 1 - ((1 - C) * (1 - I) * (1 - A))

const impact = (S) => {
    if (S === 0)
        return 6.42 * impactBase
    else if (S === 1)
        return 7.52 * (impactBase - 0.029) - 3.25
            * Math.pow((impactBase - 0.02), 15)
}

// const getS = () => 


// Базовый вектор уязвимости: AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:H

const baseScore = (impact, S, exploitability) => {
    if (impact <= 0)
        return 0
    else if (S === 0)
        return roundUp(Math.min((impact + exploitability), 10))
    else if (S === 1)
        return roundUp(Math.min((1.08 * (impact + exploitability), 10)))
}

// Таблица 14. Числовые значения временных метрик для CVSS V3
// Доступность средств эксплуатации (E):
// X 1,0
// U 0,91
// P 0,94
// F 0,97
// H 1,0
// Уровень исправления (RL)
// X 1,0
// O 0,95
// T 0,96
// W 0,97
// U 1,0
// Степень достоверности источника (RC):
// X 1,0
// U 0,92
// R 0.96
// C 1,0 

// Временной: E:P/RL:O/RC:C

const temporalScore = (baseScore, E, RL, RC) => roundUp(baseScore * E * RL * RC)

// Таблица 15. Числовые значения контекстных метрик для CVSS V3
// Скорректированный вектор атаки (MAV)
// X MAV=AV
// N 0,85
// A 0,62
// L 0,55
// P 0,2
// Скорректированная сложность атаки (MAC)
// X MAC=AC
// L 0,77
// H 0,44
// Скорректированный уровень привилегий (MPR)
// X MPR=PR
// N 0.85
// L 0,62 (0,68, если MS=1)
// H 0,27 (0,50, если MS=1)
// Скорректированное взаимодействие с пользователем (MUI):
// X MUI=UI
// N 0,85
// R 0,62
// Скорректированное влияние на другие компоненты системы (MS):
// X MS=S
// U 0
// C 1
// Скорректированное влияние на конфиденциальность (MС), Скорректированное
// влияние на целостность (MI), Скорректированное влияние на доступность (MA)
// X MC=C; MI=I; MA=A
// N 0,0
// L 0,22
// H 0,56
// Требования к конфиденциальности (CR), Требования к целостности (IR),
// Требования к доступности (AR)
// X 1,0
// H 1,5
// M 1,0
// L 0,5 


const mExploitability = (MAV, MAC, MPR, MUI) => 8.22 * MAV * MAC * MPR * MUI

const mImpactBase = (MC, CR, MI, IR, MA, AR) => Math.min((1 - (1 - MC * CR) * (1 - MI * IR) * (1 - MA * AR)), 0.915)

// const getMS = () => { }

const mImpact = (MS, mImpactBase) => {
    if (MS === 0)
        return 6.42 * mImpactBase
    if (MS === 1)
        return 7.52 * (mImpactBase - 0.029) - 3.25 * Math.pow((mImpactBase - 0.02), 15)
}

// Контекстный: CR:M/IR:M/AR:H/MAV:X/MAC:X/MPR:L/MUI:X/MS:U/MC:L/MI:L/MA:L 

const environmentalScore = (mImpact, MS, mExploitability, E, RL, RC) => {
    if (mImpact === 0)
        return 0
    if (MS === 0)
        return roundUp(
            roundUp(Math.min((mImpact + mExploitability), 10) * E * RL * RC)
        )
    if (MS === 1)
        return roundUp(
            roundUp(
                Math.min(1.08 * (mImpact + mExploitability), 10) * E * RL * RC
            )
        )
}

// const showHazardLevelFortiGuard = (score) => {
//     switch (true) {
//         case score >= 0.1 && score <= 3.9:
//             return 'Низкий'
//         case score >= 4 && score <= 6.9:
//             return 'Средний'
//         case score >= 7 && score <= 8.9:
//             return 'Высокий'
//         case score >= 9 && score <= 10:
//             return 'Критический'
//         default:
//             return 'Информационный (опасность отсутствует)'
//     }
// }