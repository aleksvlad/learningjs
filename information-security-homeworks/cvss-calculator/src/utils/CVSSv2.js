(function () {
    /**
     * Корректировка округления десятичных дробей.
     *
     * @param {String}  type  Тип корректировки.
     * @param {Number}  value Число.
     * @param {Integer} exp   Показатель степени (десятичный логарифм основания корректировки).
     * @returns {Number} Скорректированное значение.
     */
    function decimalAdjust(type, value, exp) {
        // Если степень не определена, либо равна нулю...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Если значение не является числом, либо степень не является целым числом...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Сдвиг разрядов
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Обратный сдвиг
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    // Десятичное округление к ближайшему
    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    // Десятичное округление вниз
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    // Десятичное округление вверх
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})()

// для cvss2 округление до 1 десятой
console.log('Round:' + Math.round10(4.225, -1))
// для cvss3 (округление наверх)
console.log(Math.ceil10(4.225, -1))

const roundTo1Decimal = (number) => Math.round10(number, -1)

// Вектор доступа (AV) 
// L 0,395
// A 0,646
// N 1,0
// Сложность доступа (AC)
// H 0,35
// M 0,61
// L 0,71
// Аутентификация (Au)
// M 0,45
// S 0,56
// N 0,704
// Влияние на конфиденциальность (С), Влияние на целостность (I), Влияние
// на доступность (A)
// N 0,0
// P 0,275
// C 0,66 

const impact = (C, I, A) => 10.41 * (1 - (1 - C) * (1 - I) * (1 - A))

const exploitability = (AV, AC, Au) => 20 * AV * AC * Au

const fImpact = (impact) => impact === 0 ? 0 : 1.176

// Базовый вектор уязвимости: AV:N/AC:L/Au:N/C:C/I:C/A:C 

const baseScore = (impact, exploitability, fImpact) =>
    roundTo1Decimal(((0.6 * impact) + (0.4 * exploitability) - 1.5) * fImpact)

// Числовые значения временных метрик для CVSS V2
// Возможность использования (E)
// ND 1,0
// U 0,85
// POC 0,9
// F 0,95
// H 1,0
// Уровень исправления (RL)
// ND 1,0
// OF 0,87
// TF 0,9
// W 0,95
// U 1.0
// Степень достоверности источника (RC)
// ND 1,0
// UC 0,9
// UR 0,95
// C 1,0 

// E:POC/RL:OF/RC:C

const temporalScore = (baseScore, E, RL, RC) => roundTo1Decimal(baseScore * E * RL * RC)

// Таблица 3. Числовые значения контекстных метрик для CVSS V2
// Вероятность нанесения косвенного ущерба (CDP)
// ND 0,0
// N 0,0
// L 0,1
// LM 0,3
// MH 0,4
// H 0,5
// Плотность целей (TD)
// ND 1,0
// N 0,0
// L 0,25
// M 0,75
// H 1,0
// Требования к конфиденциальности (CR); Требования к целостности (IR);
// Требования к доступности (AR)
// ND 1,0
// L 0,5
// M 1,0
// H 1,51 

const adjustedImpact = (C, CR, I, IR, A, AR) => Math.min(10, 10.41 * (1 - (1 - C * CR) * (1 - I * IR) * (1 - A * AR)))

const adjustedBaseScore = (adjustedImpact, exploitability, fImpact) => (((0.6 * adjustedImpact) + (0.4 * exploitability) - 1.5) * fImpact)

const adjustedTemporal = (adjustedBaseScore, E, RL, RC) => adjustedBaseScore * E * RL * RC

// CDP:H/TD:M/CR:M/IR:M/AR:H

const environmentalScore = (adjustedTemporal, CDP, TD) => roundTo1Decimal((adjustedTemporal + (10 - adjustedTemporal) * CDP) * TD)

const calculateOverallScore = (baseScore,environmentalScore,temporalScore) => {
    if (temporalScore !== undefined)
        return temporalScore
    if (environmentalScore !== undefined)
        return environmentalScore
    if (baseScore !== undefined)
        return baseScore
    return undefined
}

const showHazardLevelFortiGuard = (score) => {
    switch (true) {
        case score >= 0.1 && score <= 3.9:
            return 'Низкий'
            // rgba(171, 223, 79, 1)
        case score >= 4 && score <= 6.9:
            return 'Средний'
            // rgba(228, 224, 79, 1)
        case score >= 7 && score <= 8.9:
            return 'Высокий'
            // rgba(255, 119, 79, 1)
        case score >= 9 && score <= 10:
            return 'Критический'
            // rgba(255, 61, 79, 1)
        default:
            return 'Информационный (опасность отсутствует)'
            // rgba(0, 223, 79, 1)
    }
}

export { impact, exploitability, fImpact, baseScore, temporalScore, adjustedBaseScore, adjustedImpact, adjustedTemporal, environmentalScore, showHazardLevelFortiGuard, calculateOverallScore}