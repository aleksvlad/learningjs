let Sd, Su, P, V, T, A, length

P = Math.pow(10, -5)
V = 3
T = 10

Sd = Math.ceil((V * T) / P)

console.log("Нижняя граница: " + Sd)
//  Sd <= Su = A^length

// Латинские прописные (A-Z) и русские строчные (а-я)
A = 33 + 26
length = 1

while (Sd >= Math.pow(A, length)) {
    console.log(Sd)
    console.log(Math.pow(A, length))
    length += 1
}

console.log("Мощность алфавита: " + A)
console.log("Минимальная длина пароля: " + length)

Su = Math.pow(A, length)

console.log("Верхняя граница: " + Su)

LatUPPERCASE = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
    'length', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
KirLOWERCASE = []

for (let i = 1072; i < 1104; i += 1) {
    let char = String.fromCharCode(i)
    KirLOWERCASE.push(char)
}

let dictionary = LatUPPERCASE.concat(KirLOWERCASE)
console.log(dictionary)

const generatePassword = (length, dictionary) => {
    let password = ''

    for (let i = 0; i < length; i += 1) {
        password += dictionary[Math.floor(Math.random() * dictionary.length)]
    }

    return password
}

password = generatePassword(length, dictionary)
console.log("Сгенерированный пароль: " + password)

// рассчитать время брудфорсинга
H = length * Math.log2(A)
console.log('Энтропия на один символ, H, биты: ' + H)
timeForBruteForce = Math.ceil(Math.pow(A, password.length) / V)
// https://howsecureismypassword.net/ - 3000microsec
// https://password.kaspersky.com/ru/ - 2 минуты
console.log('С указанной в условии скоростью за ' + timeForBruteForce + ' минут, можно подобрать пароль перебором')
console.log('или ' + timeForBruteForce / (60 * 24) + ' дней')

console.log('------------------------------------ \n')


const nextLetter = (letter) => {

    let char = a.charCodeAt(letter);

    switch (char) {
        case 90: return 'A'
        case 122: return 'a'
        default: return letter.substring(0, letter.length - 1)
            + String.fromCharCode(letter.charCodeAt(letter.length - 1) + 1)
    }
}

const prevPrevLetter = (letter) => {
    let char = a.charCodeAt(letter);

    switch (char) {
        case 'A': return String.fromCodePoint(90)
        case 'a': return String.fromCodePoint(122)
        default: return letter.substring(0, letter.length - 1)
            + String.fromCharCode(letter.charCodeAt(letter.length - 1) - 2)
    }
}

const prevLetter = (letter) => {
    let char = a.charCodeAt(letter);

    switch (char) {
        case 'A': return String.fromCodePoint(90)
        case 'a': return String.fromCodePoint(122)
        default: return letter.substring(0, letter.length - 1)
            + String.fromCharCode(letter.charCodeAt(letter.length - 1) - 1)
    }
}


const parityOrNotCheck = (word) => {
    if (word.length % 2 !== 0) {
        console.log(word[(word.length - 1) / 2])
        return nextLetter(word[(word.length - 1) / 2])
    } else {
        console.log(word[(word.length / 2) - 1])
        return prevLetter(word[(word.length / 2) - 1])
    }
}

const countLengthAndGetChar = (word1, word2, word3) => {
    const length = word1.length + word2.length + word3.length
    // 97 a - 122 z 
    if (length <= 26) {
        return String.fromCharCode(96 + word1.length + word3.length + 1)
    } else {
        return String.fromCharCode(96 + length % 26)
    }
}

const createPassword = (firstWord, secondWord, firdWord) => {
    let result

    if (firstWord.length === 0 && secondWord.length === 0 && firdWord.length === 0) {
        console.log('Заполните все поля')
        alert('Заполните все поля')
        return
    } else {
        let passChar1 = nextLetter(firstWord[1])
        result += passChar1


        if (secondWord.length < 2) {
            alert('Второе слово должно состоять как минимум из 2 букв')
        }

        let passChar2 = prevPrevLetter(secondWord[secondWord.length - 1])
        result += passChar2

        let passChar3 = parityOrNotCheck(firdWord)
        result += passChar3

        let passChar4 = countLengthAndGetChar(firstWord, secondWord, firdWord)
        result += passChar4

        console.log(result)
        return result
        // document.getElementById("password").value = randomPassword;
    }
}

const auth = () => {
    let passwordUser = document.getElementById('passwordUser').value;
    let machine_passord = document.getElementById('password').value;
    if (machine_passord.length === 0) {
        alert('Для начала сгенерируйте пароль и только потом сравните')
    } else {
        let answer = document.getElementById('checkPassword')
        if (passwordUser === machine_passord) {
            answer.value = 'Пароль верный'
        } else {
            answer.value = 'Пароль неверный'
        }
    }
}

    // if (first.charAt(0) == 'z') {
    //     resultArray[0] = 'a'
    //     resultArray[1] = 'a'
    // } else {
    //     resultArray[0] = (char)((int) first.charAt(0) + 1);
    //     resultArray[1] = (char)((int) first.charAt(0) + 1);
    // }

// «scleroses»,
// «scoliosis», «paradantoz».
// Составить программу, которая записывает
// пароль следующим образом 

// Исходные данные — строковые константы
// 1. В строку <результат> в качестве первого символа
// записать букву, которая в алфавите следует за буквой,
// являющейся вторым символом первого слова на
// экране; если это буква «z», записать «а».
// 2. В качестве второго символа записать букву, которая
// в алфавите предшествует предпоследней букве,
// являющейся последним символом второго слова на
// экране; если это буква «а», записать «z».
// 3. Если третье слово содержит нечетное количество
// букв, то в качестве третьего символа записать букву,
// которая в алфавите следует за буквой, являющейся
// предшественником среднего символа третьего слова;
// если это буква «z», записать «а». Если же третье слово
// содержит четное количество символов, то в качестве
// третьего символа записать букву, которая в алфавите
// предшествует букве, являющейся первым из двух
// средних символов третьего слова; если это буква «а»,
// записать «z».
// 4. В качестве четвертого символа записать букву,
// которая в алфавите стоит на месте, соответствующем
// сумме количеств символов в первом и третьем словах
// плюс 1 символ;
// если эта сумма больше 26, найти и использовать в
// качестве номера позиции искомой буквы в алфавите
// остаток от деления указанной суммы

// Дополнить полученную
// программу средствами
// аутентификации
// 1. Ввести пароль пользователя. При вводе пароля
// пользователя обеспечить ввод пароля с отображением
// вместо каждого символа знаков «*».
// 2. Сравнить пароль пользователя с паролем,
// вычисленным ЭВМ.
// 3. Вывести результат аутентификации: пароль верен
// или неверен?

// KirLOWERCASE 1072..1103
// console.log(String.fromCharCode(1072))
// while(s)
// /[\u0400-\u04FF]/