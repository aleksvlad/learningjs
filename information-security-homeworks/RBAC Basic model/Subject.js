'use strict'

// User

export class Subject {

    constructor(login, password, roles) {
        this.login = login
        this.password = password
        this.roles = roles
    }

    // геттер
    get login() {
        return `${this.login}`
    }

    // сеттер
    set login(newValue) {
        [this.login] = newValue.split(' ')
    }

    get getRoles() {
        return `${this.roles}`
    }

    set setRoles(roles) {
        this.roles = roles
    }

    // вычисляемое название метода
    ["test".toUpperCase()]() {
        alert("PASSED!")
    }

}