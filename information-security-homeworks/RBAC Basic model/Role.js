'use strict'

export class Role {

    constructor(role, permisions) {
        this.role = role
        this.permisions = permisions
    }

    // геттер
    get role() {
        return `${this.role}`
    }

    get getPermisions() {
        return `${this.permisions}`
    }

    set setPermisions(permisions) {
        this.permisions = permisions
    }

    // сеттер
    // set login(newValue) {
    //   [this.login] = newValue.split(' ');
    // }

    // вычисляемое название метода
    ["test".toUpperCase()]() {
        alert("PASSED!")
    }

}