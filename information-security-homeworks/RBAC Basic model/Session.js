'use strict'

export class Session {

    constructor(user, sessionStart) {
        this.user = user
        this.sessionStart = sessionStart
    }

    // геттер
    get sessionStart() {
        return `${this.sessionStart}`
    }

    // вычисляемое название метода
    ["test".toUpperCase()]() {
        alert("PASSED!")
    }

}