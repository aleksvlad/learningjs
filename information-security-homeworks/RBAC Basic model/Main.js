'use strict'
// import { Subject } from "./Subject"
// import { Permission } from "./Permission"
// import { Role } from "./Role"
// import { Session } from "./Session"

class Subject {

    constructor(login, password, roles) {
        this.login = login
        this.password = password
        this.roles = roles
    }

    // геттер
    get login() {
        return `${this.login}`
    }

    // сеттер
    set login(newValue) {
        [this.login] = newValue
    }

    get getRoles() {
        return `${this.roles}`
    }

    set setRoles(roles) {
        this.roles = roles
    }

    // вычисляемое название метода
    ["test".toUpperCase()]() {
        alert("PASSED!")
    }

}

class Permission {

    constructor(permission) {
        this.permission = permission
    }

    // геттер
    get permission() {
        return `${this.permission}`
    }

    set setPermission(permission) {
        this.permission = permission
    }
    // вычисляемое название метода
    ["test".toUpperCase()]() {
        alert("PASSED!")
    }

}

class Role {

    constructor(role, permisions) {
        this.role = role
        this.permisions = permisions
    }

    // геттер
    get role() {
        return `${this.role}`
    }

    get getPermisions() {
        return `${this.permisions}`
    }

    set setPermisions(permisions) {
        this.permisions = permisions
    }

    // сеттер
    // set login(newValue) {
    //   [this.login] = newValue.split(' ');
    // }

    // вычисляемое название метода
    ["test".toUpperCase()]() {
        alert("PASSED!")
    }

}

class Session {

    constructor(user, sessionStart) {
        this.user = user
        this.sessionStart = sessionStart
    }

    // геттер
    get sessionStart() {
        return `${this.sessionStart}`
    }

    // вычисляемое название метода
    ["test".toUpperCase()]() {
        alert("PASSED!")
    }

}

// permissionText
let permisionToRead = new Permission("READ")
let permisionToWrite = new Permission("WRITE")
let permisionToEditUserRole = new Permission("MANAGE ROLES")
let permisionToAddEntity = new Permission("ADD ENTITY")
let permisionToDeleteEntity = new Permission("DELETE ENTITY")

let permissions = [permisionToRead, permisionToWrite, permisionToEditUserRole, permisionToAddEntity, permisionToDeleteEntity]

// roleName, permisions
// у роли админа весь список разрешеней
let admin = new Role("ADMIN", permissions)
let moderator = new Role("MODERATOR", [permisionToRead, permisionToWrite, permisionToAddEntity])
let user = new Role("USER", [permisionToRead, permisionToWrite])
let guest = new Role("GUEST", [])

let roles = [admin, moderator, user, quest]


let userAdmin = new Subject("admin", "admin", admin)
// login, password, roles
let users = []

// subject, sesionStart
let sesions = []

const registration = (login, password, roles) => {
    users.push(new Subject(login, password, roles))
}

const login = (login, password) => {
    let user = users.find(user => user.login === login && user.password === password)
    if (user) {
        console.log("User auth success")
        console.log(new Date.now())
        sesions.push(new Session(user,new Date.now()))
    } else {
        console.log("User not found or your input incorrect auth data")
    }
}

let messages = []

const write = (user, message) => {
    if (user.roles.find(role => role.role !== "GUEST")) {
        console.log(user + " " + message)
        messages.push(message)
    }
}

const read = (user) => {
    if (user.roles.find(role => role.role !== "GUEST")) {
        console.log(user + " , вы можете прочитать данный раздел если хотите. И его сообщения")
        console.log(messages)
    }
}

const addEntity = (user) => {
    if (user.roles.find(role => role.role === "ADMIN" || role.role === "MODERATOR")) {
        console.log(user + "вы можете добавлять сущности (разделы)")
    }
}

const deleteEntity = (user) => {
    if (user.roles.find(role => role.role === "ADMIN")) {
        console.log(user + "вы можете удалять сущности (разделы)")
    }
}

const manageRoles = (user) => {
    if (user.roles.find(role => role.role === "ADMIN")) {
        console.log(user + "вы можете управлять ролями пользователей")
    }
 }

registration("admin","admin",admin)
registration("moderator","moderator",moderator)
registration("user1","user1",user)
registration("WhoAmI","",guest)

login("admin","admin")
login("moderator","moderator")
login("user1","user1")
login("WhoAmI","")

// В модели присутствуют:
//  пользователи
//  роли
//  объекты
// Рассмотрим модель на примере MOODLE. Роли в MOODLE:
// Manager – может создавать и изменять курсы.
// Создатели курса – может создать курс и преподавать в нем.
// Преподаватель – может делать все внутри курса, но не может
// создать курс сам.
// Non-editing teacher – (ассистент) может преподавать, но не
// может изменять курс.
// Студент – может обучатся на курсе (записанный на курс).
// Guest – пользователь без авторизации.
// Authenticated user – пользователь, прошедший аутентификацию
// (не записанный на курс).
// Роли могут быть глобальными или «локальными».
// Типы контекста, где роли могут быть назначены:
// Manager – Система, Категория, Курс.
// Создатели курса – Система, Категория.
// Преподаватель – Курс, Модуль элемента курса.
// Non-editing teacher – Курс, Модуль элемента курса.
// Студент – Курс, Модуль элемента курса.
// Guest – нигде.
// Authenticated user – нигде. 