'use strict'

export class Permission {

    constructor(permission) {
        this.permission = permission
    }

    // геттер
    get permission() {
        return `${this.permission}`
    }

    // вычисляемое название метода
    ["test".toUpperCase()]() {
        alert("PASSED!")
    }

}