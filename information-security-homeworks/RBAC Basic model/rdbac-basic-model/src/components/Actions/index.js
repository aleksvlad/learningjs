import React from 'react'
import { List, message, Avatar, Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';

// users = 

class Actions extends React.Component {
 
    state = {
      data: [],
      loading: false,
      hasMore: true,
    };
  
    componentDidMount() {

    }
  

    handleInfiniteOnLoad = () => {
      let { data } = this.state;
      this.setState({
        loading: true,
      });
      if (data.length > 14) {
        message.warning('Infinite List loaded all');
        this.setState({
          hasMore: false,
          loading: false,
        });
        return;
      }
    
    };
  
    render() {
      return (
  
        <div className="demo-infinite-container">
          <InfiniteScroll
            initialLoad={false}
            pageStart={0}
            loadMore={this.handleInfiniteOnLoad}
            hasMore={!this.state.loading && this.state.hasMore}
            useWindow={false}
          >
            <List
              dataSource={this.state.data}
              renderItem={item => (
                <List.Item key={item.id}>
                  <List.Item.Meta
                    avatar={
                      <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                    }
                    title={<a href="https://ant.design">{item.name.last}</a>}
                    description={item.email}
                  />
                  <div>Content</div>
                </List.Item>
              )}
            >
              {this.state.loading && this.state.hasMore && (
                <div className="demo-loading-container">
                  <Spin />
                </div>
              )}
            </List>
          </InfiniteScroll>
        </div>
      );
    }
  }

export default Actions

// const write = (user, message) => {
//     if (user.roles.find(role => role.role !== "GUEST")) {
//         console.log(user + " " + message)
//         messages.push(message)
//     }
// }

// const read = (user) => {
//     if (user.roles.find(role => role.role !== "GUEST")) {
//         console.log(user + " , вы можете прочитать данный раздел если хотите. И его сообщения")
//         console.log(messages)
//     }
// }

// const addEntity = (user) => {
//     if (user.roles.find(role => role.role === "ADMIN" || role.role === "MODERATOR")) {
//         console.log(user + "вы можете добавлять сущности (разделы)")
//     }
// }

// const deleteEntity = (user) => {
//     if (user.roles.find(role => role.role === "ADMIN")) {
//         console.log(user + "вы можете удалять сущности (разделы)")
//     }
// }

// const manageRoles = (user) => {
//     if (user.roles.find(role => role.role === "ADMIN")) {
//         console.log(user + "вы можете управлять ролями пользователей")
//     }
//  }