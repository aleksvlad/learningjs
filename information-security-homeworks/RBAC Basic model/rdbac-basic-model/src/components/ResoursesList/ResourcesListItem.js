import React, { useCallback } from 'react'
import { Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons'

const ResourcesListItem = ({ res }) => {
    return (
        <div className='parking-places'>
            <span>{res}</span>
        </div>
    )
}

export default ResourcesListItem

