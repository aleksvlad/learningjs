import React from 'react'
import { List, message, Avatar, Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import ResourcesListItem from './ResourcesListItem';

// users = 

class ResourcesList extends React.Component {
 
    state = {
        data: [],
        loading: false,
        hasMore: true,
        roles: this.props.resources,
        onSelect: ''
    };

    componentDidMount() {
        console.log(this.props.resources)
        console.log(this.props)
        this.setState({
            roles: this.props.resources
        })
        // this.setState({
        //     userOnClick: this.props.userOnClick
        // })
    }
    
    handleInfiniteOnLoad = () => {
        let { data } = this.state;
        this.setState({
            loading: true,
        });
        if (data.length > 14) {
            message.warning('Infinite List loaded all');
            this.setState({
                hasMore: false,
                loading: false,
            });
            return;
        }
    };

    render() {
        // const userOnClick = this.props.userOnClick
        return (

            <div className="demo-infinite-container">
                <InfiniteScroll
                    initialLoad={false}
                    pageStart={0}
                    loadMore={this.handleInfiniteOnLoad}
                    hasMore={!this.state.loading && this.state.hasMore}
                    useWindow={false}
                >
                    <List
                        dataSource={this.state.resources}
                        renderItem={res => (
                            <ResourcesListItem res={res} />
                        )}
                    >
                        {this.state.loading && this.state.hasMore && (
                            <div className="demo-loading-container">
                                <Spin />
                            </div>
                        )}
                    </List>
                </InfiniteScroll>
            </div>
        );
    }
}
export default ResourcesList