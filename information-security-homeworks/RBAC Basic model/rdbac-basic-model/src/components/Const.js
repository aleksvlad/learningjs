export const roles = ['ADMIN', 'USER', 'GUEST', 'MODERATOR']
export const permisions = ['READ', 'WRITE', 'MANAGE RIGHT', 'ADD ENTITY', 'DELETE ENTITY', 'BLOCK USER']
export const actionsOrResource = ['ADMIN_PANEL','ADD ROLE', 'DELETE ROLE','WRITE','READ','ADD ENTITY', 'DELETE ENTITY','BLOCK USER']

export const rolesMap = {
    ADMIN: permisions,
    USER: [permisions[0], permisions[1], permisions[3]],
    GUEST: [permisions[0]],
    MODERATOR: [permisions[-2],permisions[-1]]
}

// let permisionToRead = new Permission("READ")
// let permisionToWrite = new Permission("WRITE")
// let permisionToEditUserRole = new Permission("MANAGE ROLES")
// let permisionToAddEntity = new Permission("ADD ENTITY")
// let permisionToDeleteEntity = new Permission("DELETE ENTITY")