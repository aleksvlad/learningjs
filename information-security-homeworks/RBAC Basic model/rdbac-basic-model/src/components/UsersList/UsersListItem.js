import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { List } from 'antd';
import { Button } from 'antd'
import { UserOutlined } from '@ant-design/icons'

const UsersListItem = ({ userName, userId, userOnClick }) => {
console.log({ userName, userId, userOnClick })
return (
        <List.Item key={userId}>
            <List.Item.Meta
            //                  title={<a href="https://ant.design">{item.name.last}</a>}
            //                    description={item.email}
            />
            <div className='users-list-item'>
                <Button onClick={userOnClick.bind(this, userId)} id={userId} type='primary'><UserOutlined />
                    <span>{userName}</span>
                </Button>
            </div>
        </List.Item>
    )
}



export default UsersListItem