import React from 'react'
import { List, message, Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import UsersListItem from './UsersListItem'

// users = 

class UsersList extends React.Component {

    state = {
        data: [],
        loading: false,
        hasMore: true,
        users: this.props.users,
        onSelect: ''
    };

    componentDidMount() {
        console.log(this.props.users)
        console.log(this.props)
        this.setState({
            users: this.props.users
        })
        this.setState({
            userOnClick: this.props.userOnClick
        })
    }
    
    handleInfiniteOnLoad = () => {
        let { data } = this.state;
        this.setState({
            loading: true,
        });
        if (data.length > 14) {
            message.warning('Infinite List loaded all');
            this.setState({
                hasMore: false,
                loading: false,
            });
            return;
        }
    };

    render() {
        const userOnClick = this.props.userOnClick
        return (

            <div className="demo-infinite-container">
                <InfiniteScroll
                    initialLoad={false}
                    pageStart={0}
                    loadMore={this.handleInfiniteOnLoad}
                    hasMore={!this.state.loading && this.state.hasMore}
                    useWindow={false}
                >
                    <List
                        dataSource={this.state.users}
                        renderItem={user => (
                            <UsersListItem userId={user.id}
                                userName={user.name}
                                userOnClick={userOnClick}
                            />
                        )}
                    >
                        {this.state.loading && this.state.hasMore && (
                            <div className="demo-loading-container">
                                <Spin />
                            </div>
                        )}
                    </List>
                </InfiniteScroll>
            </div>
        );
    }
}

export default UsersList