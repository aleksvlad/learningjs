import React, { useCallback } from 'react'
import { Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons'

const PermisionsListItem = ({ perm }) => {
    return (
        <div className='parking-places'>
            <span>{perm}</span>
        </div>
    )
}

export default PermisionsListItem

