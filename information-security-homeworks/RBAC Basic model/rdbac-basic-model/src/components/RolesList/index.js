import React from 'react'
import { List, message, Avatar, Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import RolesListItem from './RolesListItem';

// users = 

class RolesList extends React.Component {
 
    state = {
        data: [],
        loading: false,
        hasMore: true,
        roles: this.props.roles,
        onSelect: ''
    };

    componentDidMount() {
        console.log(this.props.roles)
        console.log(this.props)
        this.setState({
            roles: this.props.roles
        })
        // this.setState({
        //     userOnClick: this.props.userOnClick
        // })
    }
    
    handleInfiniteOnLoad = () => {
        let { data } = this.state;
        this.setState({
            loading: true,
        });
        if (data.length > 14) {
            message.warning('Infinite List loaded all');
            this.setState({
                hasMore: false,
                loading: false,
            });
            return;
        }
    };

    render() {
        // const userOnClick = this.props.userOnClick
        return (

            <div className="demo-infinite-container">
                <InfiniteScroll
                    initialLoad={false}
                    pageStart={0}
                    loadMore={this.handleInfiniteOnLoad}
                    hasMore={!this.state.loading && this.state.hasMore}
                    useWindow={false}
                >
                    <List
                        dataSource={this.state.roles}
                        renderItem={role => (
                            <RolesListItem role={role} />
                        )}
                    >
                        {this.state.loading && this.state.hasMore && (
                            <div className="demo-loading-container">
                                <Spin />
                            </div>
                        )}
                    </List>
                </InfiniteScroll>
            </div>
        );
    }
}

export default RolesList