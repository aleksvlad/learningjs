import React, { useCallback } from 'react'
import { Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons'

const RolesListItem = ({ role }) => {
    return (
        <div className='parking-places'>
            <span>{role}</span>
        </div>
    )
}

export default RolesListItem

