import { createContext } from 'react'

export const RbacContext = createContext({
    users: [{
        id: 0,
        name: 'user0',
        roles: ['Admin','Moderator']
    }]
    // clocks: [{
    //     id: 0,
    //     TimeZone: new Date().getTimezoneOffset() * (-1 / 60)
    // }]
})

export const RbacConsumer = RbacContext.Consumer

export default RbacContext
