import React, { useState, useEffect, useCallback } from 'react';
import { RbacContext } from './contexts/rbac-context'
import './App.css';
import { roles as rolesList, rolesMap, permisions as permissinsList, actionsOrResource } from './components/Const'
import UsersList from './components/UsersList';
import RolesList from './components/RolesList';
import PermisionsList from './components/PermisionsList';
import ResourcesList from './components/ResoursesList';
import Actions from './components/Actions';
import RolesListItem from './components/RolesList/RolesListItem';
import PermisionsListItem from './components/PermisionsList/PermisionsListItem';

const usersList = [
  {
    id: 0,
    name: 'user0',
    roles: ['ADMIN']
  },
  {
    id: 1,
    name: 'user1',
    roles: ['MODERATOR']
  },
  {
    id: 2,
    name: 'user2',
    roles: ['GUEST']
  },
  {
    id: 3,
    name: 'user3',
    roles: ['USER']
  },
  {
    id: 4,
    name: 'user4',
    roles: ['USER', 'MODERATOR']
  }
]

// export const permisions = ['READ', 'WRITE', 'MANAGE RIGHT', 'ADD ENTITY', 'DELETE ENTITY', 'BLOCK USER']
// export const actionsOrResource = ['ADMIN_PANEL','ADD ROLE', 'DELETE ROLE','WRITE','READ','ADD ENTITY', 'DELETE ENTITY','BLOCK USER']

const App = () => {
  const [users, setUsers] = useState(usersList)

  const [currentUser, setCurrentUser] = useState('')
  const [roles, setRoles] = useState('')
  const [permisions, setPermissions] = useState('')

  console.log(currentUser)

  useEffect(() => {
    if (currentUser !== '' && currentUser !== undefined) {
      setRoles(currentUser.roles)
      const perm = []

      roles.map(role => {
        console.log(role)
        perm.push(rolesMap.role)
      })

      setPermissions(perm)
    }
  }, [users, currentUser, roles])


  const selectUser = useCallback(
    (id) => {
      setCurrentUser(users[id])
      setRoles(users[id].roles)
    }
  )

  let res;

  if (currentUser !== '' && currentUser !== undefined) {
    console.log(currentUser)
    const rol = currentUser.roles
    res = <div style={{ overflow: 'hidden;' }}>
      <div style={{ width: '1000%;' }}>
        <span>{currentUser.name}</span>
        <div style={{ float: 'left', width: '100px', height: '100px;' }}>
          {rol.map(role => <RolesListItem role={role} />)}
        </div>
        <div style={{ float: 'left', width: '100px', height: '100px;' }}>
          {
            permissinsList.map(permision =>
              <PermisionsListItem perm={permision} />)
          }</div>
      </div>
    </div>

    console.log(res)
  } else {
    res = <div><span>Выберите юзера (логика авторизации)</span></div>
  }

  return (
    <RbacContext.Provider value={{
    }}>
      <div className="App">
        <div style={{ overflow: 'hidden;' }}>
          <div style={{ width: '1000%;' }}>
            <UsersList style={{ float: 'left', width: '100px', height: '100px;' }} users={users} userOnClick={selectUser} />
            <RolesList style={{ float: 'left', width: '100px', height: '100px;' }} roles={rolesList} />
            <PermisionsList style={{ float: 'left', width: '100px', height: '100px;' }} permissinsList={permissinsList} />
            <ResourcesList style={{ float: 'left', width: '100px', height: '100px;' }} />
            <Actions style={{ float: 'left', width: '100px', height: '100px;' }} />
          </div>
        </div>
        {res}
      </div>
    </RbacContext.Provider>
  )
}

export default App
