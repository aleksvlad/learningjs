import React, { useEffect } from 'react'
import AddAuthorForm from '../../containers/authors-containers/AddAuthorFormContainer'
import AuthorsList from '../../containers/authors-containers/authors-list-containers/AuthorsListContainer'

const AuthorsWrapper = ({ fetchAuthors }) => {

    useEffect(() => {
        fetchAuthors()
    }, [])

    return (
        <div className='authors-list-wrapper'>
            <AuthorsList />
            <AddAuthorForm />
        </div>
    )
}

export default AuthorsWrapper