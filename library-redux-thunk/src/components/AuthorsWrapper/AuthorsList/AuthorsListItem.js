import React, { useCallback } from 'react'

export const AuthorsListItem = ({ id, name, deleteAuthorButtonHandler, filterBooksByAuthorHandler }) => {

    const handleFilterClick = useCallback(e => {
        filterBooksByAuthorHandler(e, name)
    }, [filterBooksByAuthorHandler, name])

    const handleDeleteClick = useCallback(e => {
        deleteAuthorButtonHandler(e)
    }, [deleteAuthorButtonHandler])

    return (
        <li id={id}>
            <div className='list-item' onClick={handleFilterClick}>
                <b>{name}</b>
                <span className='btn-remove'
                    onClick={handleDeleteClick} />
            </div>
        </li>
    )
}

export default AuthorsListItem