import React from 'react'
import LoopCircleLoading from '../../LoopCircleLoading'
import ErrorInFetch from '../../ErrorInFetch'
import AuthorsListItem from '../../../containers/authors-containers/authors-list-containers/AuthorsListItemContainer'

const AuthorsList = ({ authors, authorFilter, pending, error }) => {

    if (error) {
        return (
            <ErrorInFetch className={'authors-list-error'}
                text={'Ошибка при получении данных...'} />
        )
    }

    if (pending) {
        return (
            <LoopCircleLoading className={'authors-list'} />
        )
    }

    return (
        <div className='authors-list'>
            <ol className='list-ol'>
                {authors.map(({ id, name }) =>
                    <AuthorsListItem id={id} key={id}
                        name={name}
                        authorFilter={authorFilter} />
                )}
            </ol>
        </div>
    )
}

export default AuthorsList