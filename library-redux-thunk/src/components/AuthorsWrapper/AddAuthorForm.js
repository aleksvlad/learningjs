import React, { useState } from 'react'
import { useFormInput, useFormSubmitButton } from '../../hooks/custom-hooks-for-form'
import '../../form.css'

const AddAuthorForm = ({ addAuthor }) => {
    const [input, setInput] = useState('')

    const authorInput = useFormInput(
        setInput, input, 'author', 'Введите имя автора'
    )

    const formSubmitButton = useFormSubmitButton(
        [input], [setInput], addAuthor, 'btn-send', 'Добавить автора'
    )

    return (
        <div>
            <form>
                <div>
                    {authorInput}
                </div>
                <div>
                    {formSubmitButton}
                </div>
            </form>
        </div>
    )
}

export default AddAuthorForm