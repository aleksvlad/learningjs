import React, { useEffect } from 'react'
import AddBooksForm from '../../containers/books-containers/add-books-form-containers/AddBookFormContainer'
import BooksList from '../../containers/books-containers/books-list-containers/BooksListContainer'

const BooksWrapper = ({ fetchBooks }) => {

    useEffect(() => {
        fetchBooks()
    }, [])

    return (
        <div className='books-list-wrapper'>
            <BooksList />
            <AddBooksForm />
        </div>
    )
}

export default BooksWrapper