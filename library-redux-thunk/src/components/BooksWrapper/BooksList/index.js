import React from 'react'
import LoopCircleLoading from '../../LoopCircleLoading'
import ErrorInFetch from '../../ErrorInFetch'
import BooksListItem from '../../../containers/books-containers/books-list-containers/BooksListItemContainer'

export const BooksList = ({ books, pending, error }) => {

    if (error) {
        return (
            <ErrorInFetch className={'authors-list-error'}
                text={'Ошибка при получении данных...'} />
        )
    }

    if (pending) {
        return (
            <LoopCircleLoading className={'authors-list'} />
        )
    }


    return (
        <div className='books-list'>
            <ol className='list-ol'>
                {books.map(({ id, bookTitle, author }) =>
                    <BooksListItem id={id} key={id}
                        bookTitle={bookTitle}
                        author={author} />
                )}
            </ol>
        </div>
    )
}

export default BooksList