import React, { useCallback } from 'react'

const BooksListItem = ({ id, bookTitle, author, deleteBookButtonHandler }) => {

    const handleDeleteClick = useCallback(e => {
        deleteBookButtonHandler(e)
    }, [deleteBookButtonHandler])

    return (
        <li id={id}>
            <div className='list-item'>
                <span><b>{bookTitle}</b><i>({author})</i></span>
                <span className='btn-remove'
                    onClick={handleDeleteClick} />
            </div>
        </li>
    )
}

export default BooksListItem