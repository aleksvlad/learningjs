import React, { useState } from 'react'
import { useForm, useField } from 'react-final-form-hooks'
import AuthorSelect from '../../../containers/books-containers/add-books-form-containers/AuthorSelectContainer'
import '../../../form.css'
import FormInput from '../../form-components/FormInput'
import SendFormButton from '../../form-components/SendFormButton'

const onSubmit = async values => {
    window.alert(JSON.stringify(values, 0, 2))
}
const validate = values => {
    const errors = {}
    if (!values.firstName) {
        errors.firstName = 'Required'
    }
    if (!values.lastName) {
        errors.lastName = 'Required'
    }
    return errors
}

export const AddBookForm = ({ addBook }) => {

    const { form, handleSubmit, values, pristine, submitting } = useForm({
        onSubmit,
        validate
    })

    const bookTitle = useField('bookTitle', form)
    const author = useField('author', form)

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <FormInput className='book' 
                    placeholder='Введите название книги'
                    field={bookTitle} />
                <div className='label-and-send'>

                    <AuthorSelect 
   //                 {/* handleOnSelect={handleOnSelect}
 //                       onSelect={handleOnSelect} */}
                        field={author}
                        //setAuthor={setAuthor}
                         />

                    <SendFormButton type='submit'
                        text='Добавить книгу'
                        disabled={submitting}/>
                </div>
            </form>
        </div>
    )
}

export default AddBookForm