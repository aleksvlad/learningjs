import React, { useState, useEffect } from 'react'
import ErrorInFetch from '../../../ErrorInFetch'
import PendingMessage from './PendingMessage'
import Select from './Select'
import '../../../../form.css'
import { PENDING, ERROR, LOADED } from '../../../../constants'

const authorSelectField = {
    PENDING: <PendingMessage />,

    ERROR: <ErrorInFetch className={'author-select-loading'}
        text={'Ошибка при получении...'} />,

    LOADED: <Select 
    // defaultValue={defaultValue}
    // handleOnSelect={handleOnSelect}
    // field={field}
    // authors={authors}
         />
}

export const AuthorSelect = ({ handleOnSelect, setAuthor, authors, field, status }) => {

    const [defaultValue, setDefaultValue] = useState('')

    useEffect(() => {
        if (authors) {
            const firstAuthor = authors.get(0)

            if (firstAuthor !== undefined) {
                setDefaultValue(firstAuthor.name)
                // setAuthor(firstAuthor.name)
            }
        }
    }, [authors])

    return (
        <label className='label-author-select'>
            <span className='author-label'> Выбор автора:</span>
            {authorSelectField.status}
        </label>
    )
}

export default AuthorSelect