import React from 'react'

const PendingMessage = () => {
    return (
        <div>
            <span className='author-select-loading'>
                <b><i>Ждем список авторов...</i></b>
            </span>
        </div>
    )
}

export default PendingMessage