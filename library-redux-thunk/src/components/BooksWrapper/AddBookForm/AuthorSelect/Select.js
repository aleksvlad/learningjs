import React from 'react'

const Select = ({ authors, defaultValue, handleOnSelect }) => {

    return (
        <select className='author-select'
            defaultValue={defaultValue}
            onChange={handleOnSelect}>
            {
                authors.map(({ id, name }) =>
                    <option id={id}
                        key={id}
                        value={name}>{name}
                    </option>
                )
            }
        </select>
    )
}

export default Select