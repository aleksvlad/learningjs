import React from 'react'
import AuthorsWrapper from '../containers/authors-containers/AuthorsWrapperContainer'
import BooksWrapper from '../containers/books-containers/BooksWrapperContainer'
import '../app.css'

const App = () => {
    return (
        <div className='app-wrapper'>
            <AuthorsWrapper />
            <BooksWrapper />
        </div>
    )
}

export default App