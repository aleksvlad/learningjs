import BooksList from '../../../components/BooksWrapper/BooksList/index'
import {
    getBooks,
    getBooksError,
    getBooksPending
} from '../../../store/features/book/index'
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({
    books: getBooks(state),
    error: getBooksError(state),
    pending: getBooksPending(state)
})

export default connect(
    mapStateToProps,
    null
)(BooksList)