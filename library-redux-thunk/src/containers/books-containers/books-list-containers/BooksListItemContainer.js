import BooksListItem from '../../../components/BooksWrapper/BooksList/BooksListItem'
import { deleteBookAction } from '../../../store/features/book/index'
import { connect } from 'react-redux'

const mapDispatchToProps = (dispatch, { id }) => ({
    deleteBookButtonHandler: (e) => {
        e.preventDefault()
        e.stopPropagation()
        dispatch(deleteBookAction(id))
    }
})

export default connect(
    null,
    mapDispatchToProps
)(BooksListItem)