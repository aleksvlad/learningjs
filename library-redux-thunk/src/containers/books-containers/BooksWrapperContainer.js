import BooksWrapper from '../../components/BooksWrapper/index'
import { fetchBooksData } from '../../store/features/book/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

const mapDispatchToProps = dispatch => ({
    fetchBooks: bindActionCreators(fetchBooksData, dispatch)
})

export default connect(
    null,
    mapDispatchToProps
)(BooksWrapper)