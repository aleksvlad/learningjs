import { addBookAction } from '../../../store/features/book/index'
import AddBookForm from '../../../components/BooksWrapper/AddBookForm/index'
import { v4 as uuidv4 } from 'uuid';
import { connect } from 'react-redux'

const mapDispatchToProps = dispatch => ({
    addBook: (bookTitle, authorName) => dispatch(
        addBookAction({
            id: uuidv4(),
            bookTitle: bookTitle,
            author: authorName
        })
    )
})

export default connect(
    null,
    mapDispatchToProps
)(AddBookForm)