import AuthorSelect from '../../../components/BooksWrapper/AddBookForm/AuthorSelect/index'
import {
    getAuthors,
    getAuthorsError,
    getAuthorsPending,
} from '../../../store/features/author/index'
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({
    error: getAuthorsError(state),
    pending: getAuthorsPending(state),
    authors: getAuthors(state)
})

export default connect(
    mapStateToProps,
    null
)(AuthorSelect)