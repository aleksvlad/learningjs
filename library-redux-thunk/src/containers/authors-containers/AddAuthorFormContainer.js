import { addAuthorAction } from '../../store/features/author/index'
import AddAuthorForm from '../../components/AuthorsWrapper/AddAuthorForm'
import { v4 as uuidv4 } from 'uuid'
import { connect } from 'react-redux'

const mapDispatchToProps = (dispatch) => ({
    addAuthor: (authorName) => dispatch(
        addAuthorAction({
            id: uuidv4(),
            name: authorName
        })
    )
})

export default connect(
    null,
    mapDispatchToProps
)(AddAuthorForm)
