import AthorsListItem from '../../../components/AuthorsWrapper/AuthorsList/AuthorsListItem'
import { filterBooksByAuthor } from '../../../store/features/book/index'
import { deleteAuthorAction } from '../../../store/features/author/index'
import { connect } from 'react-redux'

const mapDispatchToProps = (dispatch, { authorFilter, id, name }) => ({

    filterBooksByAuthorHandler: (e, authorName) => {
        e.preventDefault()
        if (authorFilter === authorName) {
            return dispatch(filterBooksByAuthor(''))
        }
        return dispatch(filterBooksByAuthor(authorName))
    },

    deleteAuthorButtonHandler: (e) => {
        e.preventDefault()
        e.stopPropagation()
        dispatch(deleteAuthorAction(id, name))
    }
})

export default connect(
    null,
    mapDispatchToProps
)(AthorsListItem)