import AthorsList from '../../../components/AuthorsWrapper/AuthorsList/index'
import { getAuthorFilterForBooks } from '../../../store/features/book/index'
import {
    getAuthors,
    getAuthorsError,
    getAuthorsPending,
} from '../../../store/features/author/index'
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({
    authors: getAuthors(state),
    error: getAuthorsError(state),
    pending: getAuthorsPending(state),
    authorFilter: getAuthorFilterForBooks(state)
})

export default connect(
    mapStateToProps,
    null
)(AthorsList)