import AuthorsWrapper from '../../components/AuthorsWrapper/index'
import { fetchAuthorsData } from '../../store/features/author/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

const mapDispatchToProps = dispatch => ({
    fetchAuthors: bindActionCreators(fetchAuthorsData, dispatch)
})

export default connect(
    null,
    mapDispatchToProps
)(AuthorsWrapper)
