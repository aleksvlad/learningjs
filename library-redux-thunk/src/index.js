import React from 'react'
import { render } from 'react-dom'
import App from './components/App'
import { Provider } from 'react-redux'
import * as serviceWorker from './serviceWorker'
import store from './store/index'

const rootElement = document.getElementById('root')

render(
    <Provider store={store} >
        <App />
    </Provider>,
    rootElement
)

serviceWorker.unregister()