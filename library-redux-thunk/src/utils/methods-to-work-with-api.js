import { fetchJSON } from './fetchJSON'
import { host } from '../constants'

export const fetchData = async (dispatch, url, requestData, receiveData, handleError) => {
    dispatch(requestData({ isFetching: true }))
    try {
        const data = await fetchJSON(url)
        dispatch(receiveData(data))
        dispatch(handleError({ didInvalidate: false }))
    } catch (error) {
        console.log(error)
        dispatch(handleError({ didInvalidate: true }))
    }
    dispatch(requestData({ isFetching: false }))
}

export const addEntity = async (dispatch, dataToAdd, url, addEntity) => {
    try {
        const entityToAdd = await fetchJSON(
            url,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(dataToAdd)
            }
        )

        dispatch(addEntity(entityToAdd))
    } catch (error) {
        console.log(error)
    }
}

export const deleteEntity = async () => {

}