import React from 'react'
import Slider from './Slider'
import css from './parking-place-view.css'
import { NavLink } from 'react-router-dom'
import phonePurpImg from '../../../resourse/images/phonepurp.svg'
// Component displayed when you 
// click on a point on the map
const ParkingPlaceView = ({ address, cost, city, photos }) => {
    return (
        <div className='space-info'>
            <Slider photos={photos} />
            <div class="space-info-row">
                <div class='space-name'>{address}</div>
                <div class='space-cost'>{cost} ₽ / час</div>
            </div>
            <div class='space-name-info'>{city}</div>
            {/* Go to booking form */}
            <div class="space-info-row bottom">
                <NavLink exact to='/parking_reserve' className='nav-item' activeClassName='nav-item-active'>
                    <span>Забронировать</span>
                </NavLink>
                <div class='space-rentor'>
                    <img src={phonePurpImg} alt='phone'/>
                    <div class='call-rentor'>
                        Связязаться с арендатором
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ParkingPlaceView