import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'
import phonePurpImg from '../../../resourse/images/phonepurp.svg'
import dotImg from '../../../resourse/images/dot1.svg'

const ParkingListItem = ({ address, empty, cost }) => {  
    return (
        <div className='parking-list-item'>
            <div className='left'>
                <img src={dotImg} alt={'Address:'}/>
                <div className='location-mark'>
                    {address}
                </div>
            </div>
            <div className='center'>
                <div className='empty'>
                    Свободно:<div className='num'>{empty}</div>
                </div>
                <div className='cost'>
                    {cost} ₽ / час
                </div>
            </div>
            <div className='right'>
                <div className='renter'>
                    <img src={phonePurpImg} alt={'phone'}/>
                </div>
                <NavLink exact to='/parking_reserve' className='nav-item' activeClassName='nav-item-active'>
                    <span>Забронировать</span>
                </NavLink>
            </div>
        </div>
    )
}

export default ParkingListItem