import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import ParkingListItem from './ParkingListItem'

const parkingInfos = [
    {
        address: 'ул. Вишневского, 15/3',
        empty:'2', 
        cost: 30, 
        city:'Казань, Республика Татарстан, Россия', 
        photos: [
            '../../../resourse/images/place-view/1.png',
            '../../../resourse/images/place-view/2.png',
            '../../../resourse/images/place-view/3.png'
        ]
    },
    {
        address: 'ул. Вишневского, 11', 
        empty:'3', 
        cost: 30, 
        city:'Казань, Республика Татарстан, Россия', 
        photos: []
    },
    {
        address: 'ул. Петербургская, 25В', 
        empty:'1', 
        cost: 35, 
        city:'Казань, Республика Татарстан, Россия', 
        photos: []
    },
    {
        address: 'ул. Кремлевская, 35', empty:'6', cost: 40, city:'Казань, Республика Татарстан, Россия', photos: []
    },
    {
        address: 'ул. Кремлевская', empty:'2', cost: 50, city:'Казань, Республика Татарстан, Россия', photos: []},
    {
        address: 'ул. Мартына Межлаука, 41-33', empty:'4', cost: 25, city:'Казань, Республика Татарстан, Россия', photos: []
    },
]

const ParkingList = ({ history }) => {
    return (
        <div className='parking-list'>
            {
                parkingInfos.map(({ address, empty, cost }) =>
                    <ParkingListItem address={address} empty={empty} cost={cost}/>            
                )
            }  
        </div>
    )
}

export default ParkingList