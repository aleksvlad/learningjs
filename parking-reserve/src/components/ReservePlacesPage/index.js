import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
// import SignInForm from '../SignInForm'
import Search from './Search'
import Map from './Map'
import ParkingList from './ParkingList/index'
import css from './reserve-place-page.css'
import ParkingPlaceView from './ParkingPlaceView'
import { useState } from 'react'

const ReservePlacesPage = ({ history }) => {

    const [ListIsHidden, setListIsHidden] = useState(false)
    const [showPlace, setShowPlace] = useState(false)

    const showPlaceHandler = () => {
        setListIsHidden(!ListIsHidden)
        setShowPlace(!showPlace)
    }

    return(
        <div className='booking'>
            <Search />
            <Map />
            { ListIsHidden ? null : <ParkingList />}
            { showPlace ? 
            <ParkingPlaceView address={'ул. Вишневского, 15/3'} 
                cost={30} 
                city={'Казань, Республика Татарстан, Россия'} 
                photos={[
                    '../../../resourse/images/place-view/1.png',
                    '../../../resourse/images/place-view/2.png',
                    '../../../resourse/images/place-view/3.png'
                ]} />
            : null }
        </div>
    )
}

export default ReservePlacesPage