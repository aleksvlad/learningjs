import React from 'react'

const AddClockButton = ({ addClock }) => {
    return (
        <button className='add-button' onClick={addClock} />
    )
}

export default AddClockButton