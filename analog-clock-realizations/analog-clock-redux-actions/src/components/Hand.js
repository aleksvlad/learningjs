import { animated } from 'react-spring'
import React, { useState, useRef, useEffect } from 'react'
import useStateWithCallback from 'use-state-with-callback'
import secondImg from '../resource/sec.png'
import minuteImg from '../resource/min.png'
import hourImg from '../resource/hour2.png'

const Hand = ({ deg, type }) => {

    const didMountRef = useRef()

    // Определяем какое изображение передать

    const selectImg = () => {
        switch (type) {
            case 'second':
                return secondImg
            case 'minute':
                return minuteImg
            case 'hour':
                return hourImg
            default:
                return secondImg
        }
    }

    const img = selectImg()

    let degToRotate = (deg === 0) ? 360 : deg

    const [style, setStyle] = useStateWithCallback({
        transition: 'transform ease 1000ms',
        transform: `rotate(${degToRotate}deg)`
    }, style => { })

    useEffect(() => {
        if (style.transform === `rotate(6deg)`) {
            console.log('SET 0')
            setStyle({
                transition: '900ms none 900ms',
                transform: `rotate(${0}deg)`,
            })
        }
    }, [style, setStyle])

    const [className] = useState(`${type}-hand`)

    useEffect(() => {
        if (!didMountRef.current) {
            didMountRef.current = true
        } else {
            if (degToRotate === 360) {

                setStyle({
                    transition: '1000ms ease transform',
                    transform: `rotate(${degToRotate}deg)`
                })

            } else {

                setStyle({
                    transition: '1000ms ease transform',
                    transform: `rotate(${degToRotate}deg)`
                })

            }
        }
    }, [degToRotate])

    return (< animated.img className={className}
        style={style}
        src={img}
        alt='second hand' />
    )

}

export default Hand