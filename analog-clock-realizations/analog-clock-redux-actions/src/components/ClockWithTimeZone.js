import React from 'react'
import Clock from './Clock'
import timeZonesList from '../utils/time-zones-list'


const ClockWithTimeZone = ({ id, timeZone, time, setTimeZone }) => {

    const getValue = (e) => parseInt(e.target.value, 10)

    console.log(timeZone)
    
    return (
        <div className='clock-with-timeZoneList'>
            <Clock
                id={id}
                time={time}
                timeZone={timeZone}
            />
            <label className='labelTimeZone'>
                Выбор часового пояса:
                <select className='timeZoneList'
                    onChange={(e) => setTimeZone(getValue(e))}
                    value={timeZone}
                >
                    {timeZonesList}
                </select>
            </label>
        </div>
    )
}

export default ClockWithTimeZone