import React, { useEffect } from 'react'
import ClockWithTimeZone from '../containers/ClockWithTimeZoneContainer'
import AddClockButton from '../containers/AddClockButtonContainer'
import DeleteClockButton from '../containers/DeleteClockButtonContainer'

const ClockList = ({ clocks, updateTime }) => {

    useEffect(() => {
        const clockInterval = setInterval(updateTime, 1000)

        return () => { clearInterval(clockInterval) }
    }, [])

    return (
        <div className='clocks'>
            {clocks.entrySeq().map(([id, timeZone]) =>
                <div key={id}>
                    <DeleteClockButton id={id} />
                    <ClockWithTimeZone
                        key={id}
                        id={id}
                    />
                </div>
            )}
            <AddClockButton />
        </div>
    )
}

export default ClockList