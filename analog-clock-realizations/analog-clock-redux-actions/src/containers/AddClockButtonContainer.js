import AddClockButton from '../components/AddClockButton'
import { addClock } from '../store/features/clock-list/index'
import { generateId, getTimezoneOffsetInHoursOnServer } from '../utils/functions-to-init-new-clock'
import { connect } from 'react-redux'

const mapDispatchToProps = dispatch => ({
    addClock: () => dispatch(
        addClock({
            id: generateId(),
            timeZone: getTimezoneOffsetInHoursOnServer()
        })
    )
})

export default connect(
    null,
    mapDispatchToProps
)(AddClockButton)