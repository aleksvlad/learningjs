import ClockList from '../components/ClockList'
import { updateTime } from '../store/features/time/index'
import getTime from '../utils/get-time-with-zero-timezone'
import { getClocks } from '../store/features/clock-list/index'
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({
    clocks: getClocks(state)
})

const mapDispatchToProps = dispatch => ({
    updateTime: () => dispatch(updateTime(getTime()))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ClockList)