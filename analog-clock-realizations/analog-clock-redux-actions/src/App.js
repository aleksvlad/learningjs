import React from 'react'
import ClockListContainer from './containers/ClockListContainer'
import './App.css'

const App = () => {
    return (
        <ClockListContainer />
    )
}

export default App