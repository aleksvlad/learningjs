import { createCache, setValue, resolveBranch } from './сache-work-methods'

const memoize = (fn) => {

    const cache = createCache()

    return (...args) => {

        // (ко всем аргументам применяем () => {}, reduce )
        const argNode = args.reduce((parentBranch, arg) => {
            return resolveBranch(parentBranch, arg)
        }, cache)

        if (argNode.hasValue) {
            return argNode.value
        }
        const value = fn(...args)

        return setValue(cache, value)
    }
}

export default memoize