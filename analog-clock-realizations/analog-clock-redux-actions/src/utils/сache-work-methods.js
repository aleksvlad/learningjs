export const createCache = () => {
    const cache = new Object()

    cache.childBranches = new WeakMap()
    cache.primitiveKeys = new Map()
    cache.hasValue = false
    cache.value = undefined

    return cache
}

const isPrimitive = (value) => {
    return ((typeof value !== 'object') && (typeof value !== 'function')) ||
        (value === null)
}

const has = (cache, key) => {
    const keyObject = (isPrimitive(key) ? cache.primitiveKeys.get(key) : key)
    return (keyObject ? cache.childBranches.has(keyObject) : false)
}

const get = (cache, key) => {
    const keyObject = (isPrimitive(key) ? cache.primitiveKeys.get(key) : key)
    return (keyObject ? cache.childBranches.get(keyObject) : undefined)
}

export const setValue = (cache, value) => {
    cache.hasValue = true
    return (cache.value = value)
}

const createKey = (cache, key) => {
    if (isPrimitive(key)) {
        const keyObject = {}
        cache.primitiveKeys.set(key, keyObject)
        return keyObject
    }
    return key
}

export const resolveBranch = (cache, key) => {

    if (cache.hasValue) { return cache }

    if (has(cache, key)) { return get(cache, key) }

    const newBranch = createCache()
    const keyObject = createKey(cache, key)

    cache.childBranches.set(keyObject, newBranch)

    return newBranch
}