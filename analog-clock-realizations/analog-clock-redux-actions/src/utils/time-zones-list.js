import React from 'react'

let timezones = []

for (let i = -12; i <= 14; i++) {
    timezones.push(
        <option key={i} value={i}>
            Часовой пояс UTC{i > 0 ? '+' : null}{i}
        </option>,
    )
}


export default timezones