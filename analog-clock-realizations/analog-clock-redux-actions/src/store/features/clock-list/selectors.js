import memoize from '../../../utils/memoize-with-weak-map-cache'

const getPathToTimeZone = memoize(id => ['clockList', id])

export const getTimeZone = (state, id) => {
    console.log(getPathToTimeZone(id))
    console.log(state)
    console.log(state.getIn(getPathToTimeZone(id)))
    return state.getIn(getPathToTimeZone(id))
}

const getPathToClockList = memoize(() => 'clockList')

export const getClocks = (state) => state.get(getPathToClockList())