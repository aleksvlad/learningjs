import { createActions } from 'redux-actions'

export const { setTimeZone, addClock, deleteClock } = createActions(
    'SET_TIME_ZONE', 'ADD_CLOCK', 'DELETE_CLOCK'
)