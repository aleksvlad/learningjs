import { handleActions } from 'redux-actions'
import { setTimeZone, addClock, deleteClock } from './actions'
import initialState from '../../inintial-state'

const clockList = handleActions(
    {
        [addClock]: (state, action) => (
            state.set(action.payload.id, action.payload.timeZone)
        ),

        [deleteClock]: (state, action) => (
            state.delete(action.payload)
        ),

        [setTimeZone]: (state, action) => (
            state.update(action.payload.id, val => action.payload.timeZone)
        )
    },
    initialState.clockList
)

export default clockList