import { handleAction } from 'redux-actions'
import { updateTime } from './actions'
import initialState from '../../inintial-state'

const time = handleAction(
    updateTime,
    (state, action) => action.payload,
    initialState
)

export default time