import memoize from '../../../utils/memoize-with-weak-map-cache'

const getPathToTime = memoize(() => 'time')

export const getTime = (state) => state.get(getPathToTime()) 