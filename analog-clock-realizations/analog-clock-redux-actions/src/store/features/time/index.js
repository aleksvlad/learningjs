import time from './reducer'

export { getTime } from './selectors'
export { updateTime } from './actions'

export default time