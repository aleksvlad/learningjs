import * as funcsToInitNewClock from '../utils/functions-to-init-new-clock'
import getTime from '../utils/get-time-with-zero-timezone'
import { OrderedMap } from 'immutable'

const idToInitialState = funcsToInitNewClock.generateId()
const timeZoneToInintialState = funcsToInitNewClock.getTimezoneOffsetInHoursOnServer()

const initialState = {
    clockList: OrderedMap([[idToInitialState, timeZoneToInintialState]]),
    time: getTime()
}

export default initialState