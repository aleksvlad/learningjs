import { combineReducers } from 'redux-immutable'
import { createStore } from 'redux'
import { fromJS } from 'immutable'
import initialState from './inintial-state'
import clockList from './features/clock-list/index'
import time from './features/time/index'

const rootReducer = combineReducers({ time, clockList })

const store = createStore(rootReducer, fromJS(initialState))

export default store