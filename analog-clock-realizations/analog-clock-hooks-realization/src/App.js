import React from 'react';
import './App.css'
import ClockList from './components/ClockList'

const App = () => {
  return (
    <div>
      <ClockList />
    </div>
  )
}

export default App;
