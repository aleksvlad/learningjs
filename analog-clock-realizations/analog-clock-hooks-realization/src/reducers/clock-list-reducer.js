import * as funcToInitClock from '../secondary-functions/functionsToInitClock'

export const clockListReducer = (state, action) => {
    switch (action.type) {
        case 'setTimeZone': {

            const { clocks } = state
            const id = action.value.id
            const newTimeZone = action.value.newTimeZone

            let newClocks = clocks.map(
                clock => (clock.id === id) ?
                    ({ id: id, timeZone: newTimeZone }) :
                    clock
            )

            return { clocks: newClocks }
        }
        case 'addClock': {
            const { clocks } = state
            const clockToAdd = {
                id: funcToInitClock.generateId(),
                timeZone: funcToInitClock.getTimezoneOffsetInHoursOnServer()
            }

            return { clocks: [...clocks, clockToAdd] }
        }
        case 'delClock': {
            const { clocks } = state
            const delId = action.value

            return {
                clocks: clocks.filter(clock => clock.id !== delId)
            }
        }
        default:
            throw new Error();
    }
}