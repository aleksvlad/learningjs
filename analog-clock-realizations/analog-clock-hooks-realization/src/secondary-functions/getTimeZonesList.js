import React from 'react'

const getTimeZonesList = () => {
    let timezones = []

    for (let i = -12; i <= 14; i++) {
        timezones.push(
            <option key={i} value={i}>
                Часовой пояс UTC{i > 0 ? '+' : null}{i}
            </option>,
        )
    }

    return timezones
}

export default getTimeZonesList