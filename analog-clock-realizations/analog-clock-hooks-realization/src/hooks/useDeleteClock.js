import { useContext } from 'react'
import { ClockContext } from '../contexts/clock-context'

const useDeleteClock = (id) => {
    const clockContext = useContext(ClockContext)
    return clockContext.delClock(id)
}

export default useDeleteClock