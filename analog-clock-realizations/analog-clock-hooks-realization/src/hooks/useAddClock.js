import { useContext } from 'react'
import { ClockContext } from '../contexts/clock-context'

const useAddClock = () => {
    const clockContext = useContext(ClockContext)
    return clockContext.addClock
}

export default useAddClock