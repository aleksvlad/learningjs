import React, { useState, useEffect, useReducer } from 'react'
import ClockWithTimeZone from './ClockWithTimeZone'
import AddClockButton from './AddClockButton'
import { ClockContext } from '../contexts/clock-context'
import DeleteClockButton from './DeleteClockButton'
import { clockListReducer } from '../reducers/clock-list-reducer'
import * as funcToInitClock from '../secondary-functions/functionsToInitClock'

const initialState = {
    clocks: [{
        id: funcToInitClock.generateId(),
        timeZone: funcToInitClock.getTimezoneOffsetInHoursOnServer()
    }]
}

const ClockList = () => {

    let time = new Date()
    time.setHours(time.getHours() - 3)

    const [curTime, setCurTime] = useState(time)
    const [{ clocks }, dispatch] = useReducer(
        clockListReducer,
        initialState
    )

    useEffect(() => {

        function setTime() {
            let time = new Date()
            time.setHours(time.getHours() - 3)
            setCurTime(time)
        }

        const timerInterval = setInterval(
            () => setCurTime(setTime),
            1000
        )
        return function cleanup() {
            clearInterval(timerInterval)
        }
    }, [])

    const addClock = () => dispatch({ type: 'addClock' })
    const delClock = (id) => () => dispatch({ type: 'delClock', value: id })
    const setTimeZone = (id, newTimeZone) => dispatch({
        type: 'setTimeZone',
        value: { id: id, newTimeZone: newTimeZone }
    })

    return (
        <ClockContext.Provider value={{
            curTime: curTime,
            clocks: clocks,
            addClock: addClock,
            delClock: delClock,
            setTimeZone: setTimeZone
        }} >
            <div className='clocks'>
                {clocks.map((clock) =>
                    <div key={clock.id.toString()}>
                        <DeleteClockButton id={clock.id.toString()}>
                        </DeleteClockButton>
                        <ClockWithTimeZone key={clock.id.toString()}
                            id={clock.id.toString()}
                        />
                    </div>
                )}
                <AddClockButton />
            </div>
        </ClockContext.Provider>
    )
}

export default ClockList