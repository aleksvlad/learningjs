import React, { useState, useContext } from 'react'
import Clock from './Clock'
import { ClockContext } from '../contexts/clock-context'
import getTimeZonesList from '../secondary-functions/getTimeZonesList'

const timeZonesList = getTimeZonesList()

const ClockWithTimeZone = (props) => {
    const [id, setId] = useState(props.id)
    const clockContext = useContext(ClockContext)
    const [curTime, clocks, setTimeZone] = [
        clockContext.curTime, 
        clockContext.clocks, 
        clockContext.setTimeZone
    ]

    const getValue = (e) => {
        return parseInt(e.target.value, 10)
    }

    const getTimeZone = (clocks, id) => {
        const index = clocks.findIndex(
            (clock) => { return clock.id === id }
        )

        return clocks[index].timeZone
    }

    return (
        <div className='clock-with-timeZoneList'>
            <Clock
                id={id} curTime={curTime}
                timeZone={getTimeZone(clocks, id)}
            />
            <label className='labelTimeZone'>
                Выбор часового пояса:
                <select className='timeZoneList'
                    onChange={
                        (event) => {
                            setTimeZone(id, getValue(event))
                        }
                    }
                    value={getTimeZone(clocks, id)}
                >
                    {timeZonesList}
                </select>
            </label>
        </div>
    )
}

export default ClockWithTimeZone