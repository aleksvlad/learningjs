import React from 'react'
import useAddClock from '../hooks/useAddClock'

const AddClockButton = () => {
    return (
        <button className='add-button'
            onClick={useAddClock()}>
        </button>
    )
}

export default AddClockButton