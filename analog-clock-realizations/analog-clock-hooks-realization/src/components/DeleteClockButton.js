import React from 'react'
import useDeleteClock from '../hooks/useDeleteClock'

const DeleteClockButton = ({ id }) => {
    return (
        <button id={id} className='del-button'
            onClick={useDeleteClock(id)}>
        </button>
    )
}

export default DeleteClockButton