import React, { useState, useEffect, useRef } from 'react'
import secondImg from '../resource/sec.png'
import minuteImg from '../resource/min.png'
import hourImg from '../resource/hour2.png'

const Hand = (props) => {

    // Определяем какое изображение передать
    const selectImg = () => {
        switch (props.type) {
            case 'second':
                return secondImg
            case 'minute':
                return minuteImg
            case 'hour':
                return hourImg
            default:
                console.log(props.type)
                return secondImg
        }
    }

    const [deg, setDeg] = useState(props.deg)
    const [img] = useState(selectImg)
    const [className, setClassName] = useState(props.type + '-hand')

    const didMountRef = useRef()

    useEffect(() => {
        if (!didMountRef.current) {
            didMountRef.current = true
        } else {
            //Обрабатываем случай перехода с 354 на 0
            if (props.deg === 0) {
                setTimeout(() => {
                    setClassName('transitionNone ' + className)
                }, 970)
            } else {
                setClassName(props.type + '-hand')
                setDeg(props.deg)
            }
        }
    }, [props.deg])

    return (
        <img className={props.deg === 0 ? className + ' transformTo360 ' : className}
            style={props.deg !== 0 ? { transform: `rotate(${deg}deg)` } : {}}
            src={img} alt='second hand' />
    )
}

export default Hand