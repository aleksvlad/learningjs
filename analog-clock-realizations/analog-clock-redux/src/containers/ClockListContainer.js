import ClockList from '../components/ClockList'
import { updateTime } from '../store/features/time/index'
import { getClocks } from '../store/features/clock-list/index'
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({
    clocks: getClocks(state)
})

const mapDispatchToProps = dispatch => ({
    updateTime: () => dispatch(updateTime())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ClockList)