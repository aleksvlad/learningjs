import DeleteClockButton from '../components/DeleteClockButton'
import { deleteClock } from '../store/features/clock-list/index'
import { connect } from 'react-redux'

const mapDispatchToProps = (dispatch, { id }) => ({
    deleteClock: () => dispatch(deleteClock(id))
})

export default connect(
    null,
    mapDispatchToProps
)(DeleteClockButton)