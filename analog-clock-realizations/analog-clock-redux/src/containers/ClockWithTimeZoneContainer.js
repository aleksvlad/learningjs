import ClockWithTimeZone from '../components/ClockWithTimeZone'
import { connect } from 'react-redux'
import { getTime } from '../store/features/time/index'
import { setTimeZone, getTimeZone } from '../store/features/clock-list/index'

const mapStateToProps = (state, { id }) => ({
    time: getTime(state),
    timeZone: getTimeZone(state, id)
})

const mapDispatchToProps = dispatch => ({
    setTimeZone: (id, newTimeZone) => dispatch(setTimeZone(id, newTimeZone))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ClockWithTimeZone)