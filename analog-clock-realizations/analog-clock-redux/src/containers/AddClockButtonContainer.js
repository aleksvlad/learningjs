import AddClockButton from '../components/AddClockButton'
import { addClock } from '../store/features/clock-list/index'
import { connect } from 'react-redux'

const mapDispatchToProps = dispatch => ({
    addClock: () => dispatch(addClock())
})

export default connect(
    null,
    mapDispatchToProps
)(AddClockButton)