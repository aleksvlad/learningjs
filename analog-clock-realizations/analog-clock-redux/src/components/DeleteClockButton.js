import React from 'react'

const DeleteClockButton = ({ id, deleteClock }) => {
    return (
        <button id={id} className='del-button' onClick={deleteClock} />
    )
}

export default DeleteClockButton