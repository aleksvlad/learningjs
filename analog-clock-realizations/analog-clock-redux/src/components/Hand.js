import React, { useState, useEffect, useRef } from 'react'
import secondImg from '../resource/sec.png'
import minuteImg from '../resource/min.png'
import hourImg from '../resource/hour2.png'

const Hand = ({ deg, type }) => {

    // Определяем какое изображение передать
    const selectImg = () => {
        switch (type) {
            case 'second':
                return secondImg
            case 'minute':
                return minuteImg
            case 'hour':
                return hourImg
            default:
                return secondImg
        }
    }

    const [img] = useState(selectImg)
    const [className, setClassName] = useState(type + '-hand')

    const didMountRef = useRef()

    useEffect(() => {
        if (!didMountRef.current) {
            didMountRef.current = true
        } else {
            //Обрабатываем случай перехода с 354 на 0
            if (deg === 0) {
                setTimeout(() => {
                    setClassName('transitionNone ' + className)
                }, 940)
            } else {
                setClassName(type + '-hand')
            }
        }
    }, [deg])

    return (
        <img
            className={
                deg === 0 ?
                    className + ' transformTo360' :
                    className
            }
            style={deg !== 0 ? { transform: `rotate(${deg}deg)` } : {}}
            src={img}
            alt='second hand' />
    )
}

export default Hand