import React from 'react'
import Hand from './Hand'

const Clock = ({ time, timeZone, id }) => {

    // 1. Полученное время сurTime в часовом поясе +0, 
    // поэтому добавляем часовой пояс данных часов.
    // 2. Далее берем mod 12, т.к. на часах 12 часов, а не 24, 
    // 3. Прибавляем текущий minDeg / 12, 
    // что бы она перемещалась плавно как на настоящих часах
    // 4. Аналогично в minDeg прибавляем secDeg / 60

    let secDeg = time.getSeconds() * 6,
        minDeg = time.getMinutes() * 6 + secDeg / 60,
        hourDeg = ((time.getHours() + timeZone) % 12)
            * 30 + minDeg / 12

    return (
        <div className='clock' id={id + 'clock'}>
            <Hand deg={secDeg} type={'second'} />
            <Hand deg={minDeg} type={'minute'} />
            <Hand deg={hourDeg} type={'hour'} />
        </div>
    )
}

export default Clock