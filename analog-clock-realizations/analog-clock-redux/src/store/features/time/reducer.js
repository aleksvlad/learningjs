import { UPDATE_TIME } from './actions'
import initialState from '../../inintial-state'

const time = (state = initialState.time, action) => {
    switch (action.type) {
        case UPDATE_TIME:
            return action.payload

        default:
            return state
    }
}

export default time