import getTime from '../../../utils/get-time-with-zero-timezone'

export const UPDATE_TIME = 'UPDATE_TIME'

export const updateTime = () => ({
    type: UPDATE_TIME,
    payload: getTime()
})