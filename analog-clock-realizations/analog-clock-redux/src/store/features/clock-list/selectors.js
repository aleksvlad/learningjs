// const getPathToTimeZone = memoize(id => ['clockList', id])
// getPathToTimeZone(id)

export const getTimeZone = (state, id) =>
    state.getIn(['clockList', id])

export const getClocks = (state) => {
    return state.get('clockList')
}
