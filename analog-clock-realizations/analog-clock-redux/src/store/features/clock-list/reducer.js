import initialState from '../../inintial-state'
import { ADD_CLOCK, DELETE_CLOCK, SET_TIMEZONE } from './actions'

const clockList = (state = initialState.clocks, action) => {
    switch (action.type) {
        case ADD_CLOCK:
            return state.set(action.payload.id, action.payload.timeZone)
        case SET_TIMEZONE:
            return state.update(action.payload.id,
                val => action.payload.timeZone)
        case DELETE_CLOCK:
            return state.delete(action.id)
        default:
            return state
    }
}

export default clockList