import { generateId, getTimezoneOffsetInHoursOnServer } from '../../../utils/functions-to-init-new-clock'

export const ADD_CLOCK = 'ADD_CLOCK'
export const DELETE_CLOCK = 'DELETE_CLOCK'
export const SET_TIMEZONE = 'SET_TIMEZONE'

export const addClock = () => ({
    type: ADD_CLOCK,
    payload: {
        id: generateId(),
        timeZone: getTimezoneOffsetInHoursOnServer()
    }
})

export const deleteClock = (id) => ({
    type: DELETE_CLOCK,
    id
})

export const setTimeZone = (id, newTimeZone) => ({
    type: SET_TIMEZONE,
    payload: { id: id, timeZone: newTimeZone }
})
