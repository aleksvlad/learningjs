export const getTimezoneOffsetInHoursOnServer = () => {
    // GetTimezoneOffset() - Вовращает смещение часового пояса 
    // относительно часового пояса UTC в минутах, преобразуем в часы
    const timeZone = new Date().getTimezoneOffset() * (-1 / 60)
    console.log(parseInt(timeZone, 10))
    return parseInt(timeZone, 10)
}

export const generateId = () => {
    return '_' + Math.random().toString(36).substr(2, 9);
}