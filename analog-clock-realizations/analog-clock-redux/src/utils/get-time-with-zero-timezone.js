const getTimeWithZeroTimeZone = () =>{
    let time = new Date()
    time.setHours(time.getHours() - 3)
    return time
}

export default getTimeWithZeroTimeZone