import React from 'react'
import ClockListContainer from './containers/ClockListContainer'
import { connect } from 'react-redux'
import './App.css'

const App = () => {
    return (
        <ClockListContainer />
    )
}

export default App