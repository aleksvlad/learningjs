export function rotateHand(hand, deg) {
    // При градусе равном 0, подменяем его на 360
    // под конец анимации, отключаем transition и устанавливаем 0
    if (deg === 0) {
        hand.style.transition = 'all ease 1000ms'
        hand.style.transform = 'rotate(360deg)'

        setTimeout(() => {
            hand.style.transition = 'none 0ms ease 0s'
            hand.style.transform = 'rotate(' + deg + 'deg)'
        }, 960)
    } else if (deg === 6) {
        hand.style.transition = 'all ease 1000ms'
        hand.style.transform = 'rotate(' + deg + 'deg)'
    } else {
        hand.style.transform = 'rotate(' + deg + 'deg)'
    }
}