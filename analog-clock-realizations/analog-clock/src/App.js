import React from 'react'
import './App.css'
import ClockList from './components/ClockList'

function App() {
    return (
        <div>
            <ClockList />
        </div>
    )
}

export default App;