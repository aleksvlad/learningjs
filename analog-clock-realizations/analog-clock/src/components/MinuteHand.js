import React from 'react'
import minuteImg from '../resource/min.png'
import { rotateHand } from '../secondary-functions/rotate-hand'

class MinuteHand extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            transform: this.props.deg
        }
    }

    minuteRef = null

    setMinuteRef = ref => {
        this.minuteRef = ref
    }

    componentDidUpdate() {
        rotateHand(this.minuteRef, this.props.deg)
    }

    render() {
        return (
            <img className='minute-hand'
                ref={this.setMinuteRef}
                style={{
                    transition: `all ease 1000ms`,
                    transform: `rotate(${this.state.transform}deg)`
                }}
                src={minuteImg} alt='minutes hand' />
        )
    }
}

export default MinuteHand