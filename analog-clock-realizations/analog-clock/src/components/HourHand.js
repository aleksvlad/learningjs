import React from 'react'
import hourImg from '../resource/hour2.png'
import { rotateHand } from '../secondary-functions/rotate-hand'

class HourHand extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            transform: this.props.deg
        }
    }

    hourRef = null
  
    setHourRef = ref => {
        this.hourRef = ref
    }

    componentDidUpdate() {
        rotateHand(this.hourRef, this.props.deg)
    }

    render() {
        return (
            <img className='hour-hand'
                ref={this.setHourRef}
                style={{
                    transition: `all ease 1000ms`,
                    transform: `rotate(${this.state.transform}deg)`
                }}
                src={hourImg} alt='hour hand' />
        )
    }
}

export default HourHand