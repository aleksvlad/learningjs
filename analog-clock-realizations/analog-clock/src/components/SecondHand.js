import React from 'react'
import secondImg from '../resource/sec.png'
import { rotateHand } from '../secondary-functions/rotate-hand'

class SecondHand extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            transform: this.props.deg
        }
    }

    secondRef = null

    setSecondRef = ref => {
        this.secondRef = ref
    }

    componentDidUpdate() {
        rotateHand(this.secondRef, this.props.deg)
    }

    render() {
        return (
            <img className='second-hand'
                ref={this.setSecondRef}
                style={{
                    transition: `all ease 1000ms`,
                    transform: `rotate(${this.state.transform}deg)`
                }}
                src={secondImg} alt='second hand' />
        )
    }
}

export default SecondHand