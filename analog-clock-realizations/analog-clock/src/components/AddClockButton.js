import React from 'react'
import { ClockContext } from '../contexts/clock-context'

function AddClockButton() {
    return (
        <ClockContext.Consumer>
            {context => (
                <button className='add-button' 
                    onClick={context.addClock}>
                </button>
            )}
        </ClockContext.Consumer>
    )
}

export default AddClockButton