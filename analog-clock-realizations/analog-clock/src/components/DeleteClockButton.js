import React from 'react'
import { ClockContext } from '../contexts/clock-context'

function DeleteClockButton({ id }) {
    return (
        <ClockContext.Consumer>
            {context => (
                <button id={id} className='del-button'
                    onClick={context.delClock.bind(this, id)}>
                </button>
            )}
        </ClockContext.Consumer>
    )
}

export default DeleteClockButton