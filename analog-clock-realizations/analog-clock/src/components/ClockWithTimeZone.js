import React from 'react'
import Clock from './Clock'
import { ClockContext } from '../contexts/clock-context'

function getTimeZonesList() {
    let timezones = []

    for (let i = -12; i <= 14; i++) {
        timezones.push(
            <option key={i} value={i}>
                Часовой пояс UTC{i > 0 ? '+' : null}{i}
            </option>,
        )
    }

    return timezones
}

const timeZonesList = getTimeZonesList()

class ClockWithTimeZone extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: props.id
        }
        this.getValue = this.getValue.bind(this)
    }

    getValue(event) {
        return parseInt(event.target.value, 10)
    }

    render() {
        return (
            <ClockContext.Consumer>
                {context => (
                    <div className='clock-with-timeZoneList'>

                        <Clock
                            id={this.state.id} curTime={context.curTime}
                            timeZone={context.getTimeZone(this.state.id)}
                        />

                        <label className='labelTimeZone'>
                            Выбор часового пояса:
                            <select className='timeZoneList'
                                onChange={
                                    (event) => {
                                        context.setTimeZone(this.state.id, this.getValue(event))
                                    }
                                }
                                value={context.getTimeZone(this.state.id)}>
                                {timeZonesList}
                            </select>
                        </label>
                    </div>
                )}
            </ClockContext.Consumer>
        )
    }
}

export default ClockWithTimeZone