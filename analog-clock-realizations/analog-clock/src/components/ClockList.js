import React from 'react'
import ClockWithTimeZone from './ClockWithTimeZone'
import AddClockButton from './AddClockButton'
import { ClockContext } from '../contexts/clock-context'
import DeleteClockButton from './DeleteClockButton'
import * as funcToInitClock from '../secondary-functions/functionsToInitClock'

class ClockList extends React.Component {

    constructor(props) {
        super(props)

        this.addClock = () => {
            const clockToAdd = {
                id: funcToInitClock.generateId(),
                timeZone: funcToInitClock.getTimezoneOffsetInHoursOnServer()
            }
            const newClocks = [...this.state.clocks, clockToAdd]

            this.setState({ clocks: newClocks })
        }

        this.delClock = (delId) => {
            const newClocks = this.state.clocks.filter(
                clock => clock.id !== delId
            )
          
            this.setState({ clocks: newClocks })
        }

        this.state = {
            curTime: new Date(),

            clocks: [
                {
                    id: funcToInitClock.generateId(),
                    timeZone: funcToInitClock.getTimezoneOffsetInHoursOnServer()
                }
            ],

            setTimeZone: (id, newTimeZone) => {

                let newClocks = this.state.clocks.map(
                    clock => (clock.id === id) ?
                        ({ id: id, timeZone: newTimeZone }) :
                        clock
                )

                this.setState({ clocks: newClocks })
            },

            getTimeZone: (id) => {
                return this.state.clocks[this.findClockIndexById(id)]
                    .timeZone
            },

            delClock: this.delClock,
            addClock: this.addClock
        }
    }

    componentDidMount() {
        this.setTime()

        this.timerInterval = setInterval(
            () => this.setTime(),
            1000
        )
    }

    setTime() {
        let time = new Date()
        time.setHours(time.getHours() - 3)
        this.setState({ curTime: time })
    }

    findClockIndexById(id) {
        return this.state.clocks.findIndex(
            (clock) => { return clock.id === id }
        )
    }

    render() {
        return (
            <ClockContext.Provider value={this.state}>
                <div className='clocks'>
                    {this.state.clocks.map((clock) =>
                        <div key={clock.id.toString()}>
                            <DeleteClockButton id={clock.id.toString()}>
                            </DeleteClockButton>
                            <ClockWithTimeZone key={clock.id.toString()}
                                id={clock.id.toString()}
                            />
                        </div>
                    )}
                    <AddClockButton />
                </div>
            </ClockContext.Provider>
        )
    }
}

export default ClockList