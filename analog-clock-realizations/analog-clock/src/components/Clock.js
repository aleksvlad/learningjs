import React from 'react'
import SecondHand from './SecondHand'
import MinuteHand from './MinuteHand'
import HourHand from './HourHand'

function Clock({ time, timeZone, id }) {

    // 1. Полученное время сurTime в часовом поясе +0, 
    // поэтому добавляем часовой пояс данных часов.
    // 2. Далее берем mod 12, т.к. на часах 12 часов, а не 24, 
    // 3. Прибавляем текущий minDeg / 12, 
    // что бы она перемещалась плавно как на настоящих часах
    // 4. Аналогично в minDeg прибавляем secDeg / 60

    let secDeg = time.getSeconds() * 6,
        minDeg = time.getMinutes() * 6 + secDeg / 60,
        hourDeg = ((time.getHours() + timeZone) % 12)
            * 30 + minDeg / 12

    return (
        <div className='clock' id={id + 'clock'}>
            <SecondHand deg={secDeg} />
            <MinuteHand deg={minDeg} />
            <HourHand deg={hourDeg} />
        </div>
    )
}

export default Clock