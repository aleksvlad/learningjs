import { createContext } from 'react'

export const ClockContext = createContext({
    clocks: [{
        id: 0,
        TimeZone: new Date().getTimezoneOffset() * (-1 / 60)
    }]
})

export const ClockConsumer = ClockContext.Consumer

export default ClockContext
