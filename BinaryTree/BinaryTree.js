class BinaryTreeNode{
   
    constructor(){
        this.left = null
        this.right = null
    }

    getLeft(){
        // if(this.left){
            return this.left || (this.left = new BinaryTreeNode())
        // }
    }

    getRight(){
        // if(this.right){
            return this.right || (this.right = new BinaryTreeNode())
        // }
    }

    // getLeftData(){
    //     if(this.left){
    //         return this.left || (this.left = 0)
    //     }
    // }

    // getRightData(){
    //     if(this.right){
    //         return this.right || (this.right = 0)
    //     }
    // }

    // setLeftData(data){
    //     this.left = data
    // }

    // setRightData(data){
    //     this.right = data
    // }

    setData(data){
        
        if(typeof data === 'function'){
           return this.data = data(this.getData())
        }
        
        return this.data = data
        
    }

    getData(){
        return this.data || (this.data = 0)
    }

}

const search = (root,path,step) =>{
    if(step === 0){
        return root
    }

    step -= 1    

    return search(
        (path &(1 << (step))) ? root.getRight() : root.getLeft(),
        path,
        step
    )

}

const clear = (root,path,step) =>{

    if(step === 0){
       return true
    } 
    
   step -= 1    

    if(path & (1 << (step))){
        if(clear(root.getRight(),path,step)){
            root.right = null
        }
    } else if (clear(root.getLeft(),path,step)){
            root.left = null
    }

    return root.left === null && root.right === null
}

// const toArray = (binaryTree) =>{
//     if (binaryTree.root === null) return
//     toArray()
// }

let arr

const toArray = (tree) => {
    
    const arr = []
    let root  = tree.root
    let number = 0
    let myroot
    
    console.log(root)

    const goToNode = (arr, root, number) => {
        
        if(root.data !== undefined){
            for(let i = 0; i < root.data; i += 1){
                arr.push(number)
            }
        }

        if (root.left) {
            goToNode(arr, root.getLeft(), number << 1)
        }

        if (root.right) {
            goToNode(arr, root.getRight(), number << 1|1)
        }
    }

        // debugger
    goToNode(arr, root, 0)
    console.log(arr)
    return arr
    
}

class BinaryTree{
    
    constructor(numbers = [],bitDepth = 32){
        this.root = new BinaryTreeNode()
        this.bitDepth = bitDepth
        numbers.forEach(number => this.insert(number))
    }

    insert(number){
        const node = search(this.root,number,this.bitDepth)
        return node.setData(value => value + 1)
            
    }

    remove(number){
        const node = search(this.root,number,this.bitDepth)
        const value = node.setData(value => value - 1)
        
        if(value === 0){
            clear(this.root,number,this.bitDepth)
        }

        return value

    }

}

const tree = new BinaryTree([1,2,3,2,4,2,5],3)
console.log(tree)
toArray(tree, 3)

console.log(tree.root)