// Рекуррентно вычислить число Фиббоначи под номером текущего месяца и вывести в консоль

let month = new Date().getMonth()

function calculateFibonacci(fib, next, month){
    let res = fib
    console.log(res)
    return month === 0 ? res : calculateFibonacci(next, fib + next, month - 1) 
}

console.log("Номер текущего месяца " + month + ", вычисленное число Фиббоначи под данным номером - " + calculateFibonacci(0, 1, month))