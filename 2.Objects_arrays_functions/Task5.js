/*Написать функцию inc, которая возвращает количество ее вызовов. 
Переменную, которая хранит количество вызовов, 
нужно хранить не в глобальной области видимости, а внутри замыкания. 
Для замыкания нужно создать функцию, которая возвращает функцию inc.*/

function counter(){

    let countCalls = 0

    return function inc(){
        return countCalls += 1
    }

}

let count = counter()

console.log(count())
console.log(count())
console.log(count())
console.log(count())
console.log(count())
