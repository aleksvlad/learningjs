/* Дан массив:

1, 2, 3, 4, 5, 6, 7, 8, 9, 10

При помощи цепочки вызовов методов произвести следующие действия:
1. Отфильтровать нечетные числа
2. Увеличить каждое число на его индекс в массиве
3. Поделить каждое значение на 3
4. Каждое значение n превратить в масив из двух элементов [n, 11 - n]
5. Получившийся массив из маленьких массивов превратить в сплошной одноуровневый массив.

Уточню:
[[a, b], [c, d]...] -> [a, b, c, d...]

6. Отсортировать получившийся массив

Вывести результат в консоль */

let arr = []

for (let i = 0; i < 10; i += 1) {
    arr[i] = i + 1
}

console.log(arr)

arr = arr.filter(a => a % 2 === 0)
console.log(arr)

arr = arr.map(a => a += arr.indexOf(a))
console.log(arr)

arr = arr.map(a => a /= 3)
console.log(arr)

arr = arr.map(a => a = [a, 11 - a])
console.log(arr)

arr = arr.shift();
console.log(arr)

// arr = concateTwoDimensionalArray(arr).sort((a,b) => a - b)
// console.log(arr)

function concateTwoDimensionalArray(arr){
   
    let resultArr = []

    for (let i = 0; i < arr.length; i += 1) {
        for (let j = 0; j < arr[i].length; j++) {
          resultArr.push(arr[i][j])
        }
    }
    return resultArr
}

// console.log(concateTwoDimensionalArray(arr.filter(a => a % 2 === 0).map(a => a += arr.indexOf(a)).map(a => a /= 3).map(a => a = [a, 11 - a])).sort((a,b) => a - b)) 