function isPrimitive(value) {
    return ((typeof value !== 'object') && (typeof value !== 'function')) ||
        (value === null)
}

function Cache() {
    this.childBranches = new WeakMap()
    this.primitiveKeys = new Map()
    this.hasValue = false
    this.value = undefined
}

Cache.prototype.has = function has(key) {
    const keyObject = (isPrimitive(key) ? this.primitiveKeys.get(key) : key)
    return (keyObject ? this.childBranches.has(keyObject) : false)
}

Cache.prototype.get = function get(key) {
    const keyObject = (isPrimitive(key) ? this.primitiveKeys.get(key) : key)
    return (keyObject ? this.childBranches.get(keyObject) : undefined)
}

Cache.prototype.resolveBranch = function resolveBranch(key) {
    if (this.has(key)) { return this.get(key) }

    const newBranch = new Cache()
    const keyObject = this.createKey(key)

    this.childBranches.set(keyObject, newBranch)

    return newBranch
}

Cache.prototype.setValue = function setValue(value) {
    this.hasValue = true
    return (this.value = value)
}

Cache.prototype.createKey = function createKey(key) {
    if (isPrimitive(key)) {
        const keyObject = {}
        this.primitiveKeys.set(key, keyObject)

        return keyObject
    }
    return key
}
export default Cache