function isPrimitive(value) {
    return ((typeof value !== 'object') && (typeof value !== 'function')) || (value === null);
}

function MapTree() {
    this.childBranches = new WeakMap();
    this.primitiveKeys = new Map();
    this.hasValue = false;
    this.value = undefined;
}

MapTree.prototype.has = function has(key) {
    var keyObject = (isPrimitive(key) ? this.primitiveKeys.get(key) : key);
    return (keyObject ? this.childBranches.has(keyObject) : false);
};

MapTree.prototype.get = function get(key) {
    var keyObject = (isPrimitive(key) ? this.primitiveKeys.get(key) : key);
    return (keyObject ? this.childBranches.get(keyObject) : undefined);
};

MapTree.prototype.resolveBranch = function resolveBranch(key) {
    console.log('\n----------resolve branch -----------')
    if (this.has(key)) {
        console.log('from cache ' + key)
        return this.get(key);
    }
    var newBranch = new MapTree();
    var keyObject = this.createKey(key);
    this.childBranches.set(keyObject, newBranch);
    console.log(keyObject)
    console.log(this.childBranches)
    console.log('----------resolve branch -----------\n')
    return newBranch;
};

MapTree.prototype.setValue = function setValue(value) {
    this.hasValue = true;
    return (this.value = value);
};

MapTree.prototype.createKey = function createKey(key) {
    if (isPrimitive(key)) {
        var keyObject = {};
        this.primitiveKeys.set(key, keyObject);
        return keyObject;
    }
    return key;
};

MapTree.prototype.clear = function clear() {
    if (arguments.length === 0) {
        this.childBranches = new WeakMap();
        this.primitiveKeys.clear();
        this.hasValue = false;
        this.value = undefined;
    } else if (arguments.length === 1) {
        var key = arguments[0];
        if (isPrimitive(key)) {
            var keyObject = this.primitiveKeys.get(key);
            if (keyObject) {
                this.childBranches.delete(keyObject);
                this.primitiveKeys.delete(key);
            }
        } else {
            this.childBranches.delete(key);
        }
    } else {
        var childKey = arguments[0];
        if (this.has(childKey)) {
            var childBranch = this.get(childKey);
            childBranch.clear.apply(childBranch, Array.prototype.slice.call(arguments, 1));
        }
    }
};

function memoize(fn) {
    var argsTree = new MapTree();

    function memoized() {
        console.log('\n__START__')
        var args = Array.prototype.slice.call(arguments);
        var argNode = args.reduce(function getBranch(parentBranch, arg) {
            return parentBranch.resolveBranch(arg);
        }, argsTree);

        console.log("arguments: ")
        console.log(arguments)
        console.log("Cache: ")
        console.log(argsTree)
        if (argNode.hasValue) {
            console.log('   ArgNode: ')
            console.log(argNode)
            console.log(argNode + ' from cache')
            console.log('   childBranches: ')
            console.log(argNode.childBranches)
            console.log('   primitive keys:')
            console.log(argNode.primitiveKeys)
            console.log('__END_FROM_CACHE__\n')
            return argNode.value;
        }
        var value = fn.apply(null, args);
        console.log(value + ' calculation')
        console.log('__END_AND_CALC__\n')
        return argNode.setValue(value);
    }

    memoized.clear = argsTree.clear.bind(argsTree);

    return memoized;
};

// let foo = { foo: true };
// let bar = { bar: true };
// let baz = { baz: true };

const fn = memoize((...args) => args); // Создаем запомненную функцию

// fn(['clockList', 'id'])
// fn(['clockList', 'id'])

let foo = ['clockList', 'id0dsd_ds'];
let bar = 'time';
let baz = 'clockList';


// fn('clockList')
// fn('clockList')

console.log(fn(foo, bar, baz)); // Возвращает [{foo: true}, {bar: true}, {baz: true}] 
console.log(fn(foo, bar, baz)); // Возвращает кешированный результат

console.log(fn('time'))
console.log(fn('time'))
console.log(fn('time'))
console.log(fn(foo, bar, baz))
bar = 'tl'
// foo = bar = baz = undefined; // Оригинал foo, bar and baz теперь может собирать мусор
console.log(fn(foo, bar, baz))
console.log(fn(foo, bar, baz))
