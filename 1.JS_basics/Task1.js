let timeMeasurements = []
let results = []
let result
let timeBefore
let timeAfter
    
let str = ""

for (let i = 0; i < 100000; i = i + 1) {

    str = randomString(10)
    timeBefore = performance.now()
    str += randomString(10)
    timeAfter = performance.now()

    if((timeAfter - timeBefore) !== 0){
        timeMeasurements.push(timeAfter - timeBefore)
    }

}

result = {
  id : "Замеры времени выполнения конкатенации двух строк",
  measurements : getAvg(timeMeasurements),
}

results.push(result)

timeMeasurements = []

let number = 10
for (let i = 0; i < 100000; i = i + 1) {
    
    timeBefore = performance.now()
    number += Math.floor(Math.random() * 100)    
    timeAfter = performance.now()

    if((timeAfter - timeBefore) !== 0){
        timeMeasurements.push(timeAfter - timeBefore);
    }
    
}

result = {
    id : "Замеры времени выполнения сложения двух чисел",
    measurements : getAvg(timeMeasurements)
}

results.push(result)

timeMeasurements = []
let concatNumber = "string"

for (let i = 0; i < 100000; i = i + 1) {
    
    timeBefore = performance.now()
    concatNumber += Math.floor(Math.random() * 100) 
    timeAfter = performance.now()

    if((timeAfter - timeBefore) !== 0){
        timeMeasurements.push(timeAfter - timeBefore)
    }

}

result = {
    id : "Замеры времени выполнения конкатенации строки и числа",
    measurements : getAvg(timeMeasurements)
}

results.push(result)
results.sort(compare)
showResult(results)


function getAvg(timeMeasurements) {
    
    let total = 0

    for(let i = 0; i < timeMeasurements.length; i++) {
        total += timeMeasurements[i]
    }

    let avg = total / timeMeasurements.length    
    return avg
}

function randomString(i) {
    let someString = ''
    while (someString.length < i) 
        someString += Math.random().toString(36).substring(2)
    return someString
};

function compare(a, b) {
    return b.measurements - a.measurements;
}

function showResult(results){
    let resultStr = ""

    for (let i = 0; i < results.length; i++){
        resultStr += i + 1 +") " + results[i].id + " - " + results[i].measurements + "\n"
    }

    console.log("Результаты выполненных тестов, в порядке возрастания скорости:\n" + resultStr)
    alert("Результаты выполненных тестов, в порядке возрастания скорости:\n" + resultStr)    
}

// Обьявление let или const;
// Обьяснить почему var не нужен, не исп. ++, пробелы, 
// Task1 -  то же только в консоли
// Task2 - один лишний символ,
// 
// Применения замыкания:
// 
// инкапсуляция ()
// каррирование *
// ...