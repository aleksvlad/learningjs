function summing(a,b) {
	if (b == 0) return a
    let sum = a ^ b 		
	let carryover = (a & b) << 1	
	return summing(sum, carryover)
}

function abs(a) {
   return a < 0 ? summing(~a,1) : a;
}

function multiply(a,b) {
    
    if(a === 0 || b === 0) return 0
    
    if (abs(a) < abs(b)) return multiply(b, a); // алгоритм будет быстрее, если b < a
    
    let result = 0

    if(b > 0){

        while(b !== 0){
    
            if (b & 1) {
                result = summing(result,a)
            }

            a = a << 1
            b = b >> 1

        }
        
        return result   
    
    } else {

        b = abs(b)

        while (b !== 0) {

            if (b & 1) {
                result = summing(result,a)
            }

            a = a << 1
            b = b >> 1

        
        }
            
        return summing(~result,1)
    }

}

let mult = multiply(20,-10)

console.log(mult)

// пока a не равно нулю:
//     если 1-й бит a не равен нулю:
//         с = с + b
//     b = b сдвинутое на один разряд влево (в сторону увеличения веса)
//     a = a сдвинутое на один разряд вправо
